import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ProjetInscriptionTestModule } from '../../../test.module';
import { AssureurComponent } from 'app/entities/assureur/assureur.component';
import { AssureurService } from 'app/entities/assureur/assureur.service';
import { Assureur } from 'app/shared/model/assureur.model';

describe('Component Tests', () => {
  describe('Assureur Management Component', () => {
    let comp: AssureurComponent;
    let fixture: ComponentFixture<AssureurComponent>;
    let service: AssureurService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetInscriptionTestModule],
        declarations: [AssureurComponent],
      })
        .overrideTemplate(AssureurComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AssureurComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AssureurService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Assureur(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.assureurs && comp.assureurs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
