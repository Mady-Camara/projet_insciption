import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetInscriptionTestModule } from '../../../test.module';
import { AnneeUniversitaireDetailComponent } from 'app/entities/annee-universitaire/annee-universitaire-detail.component';
import { AnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';

describe('Component Tests', () => {
  describe('AnneeUniversitaire Management Detail Component', () => {
    let comp: AnneeUniversitaireDetailComponent;
    let fixture: ComponentFixture<AnneeUniversitaireDetailComponent>;
    const route = ({ data: of({ anneeUniversitaire: new AnneeUniversitaire(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetInscriptionTestModule],
        declarations: [AnneeUniversitaireDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AnneeUniversitaireDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AnneeUniversitaireDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load anneeUniversitaire on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.anneeUniversitaire).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
