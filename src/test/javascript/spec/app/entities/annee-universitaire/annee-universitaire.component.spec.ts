import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { ProjetInscriptionTestModule } from '../../../test.module';
import { AnneeUniversitaireComponent } from 'app/entities/annee-universitaire/annee-universitaire.component';
import { AnneeUniversitaireService } from 'app/entities/annee-universitaire/annee-universitaire.service';
import { AnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';

describe('Component Tests', () => {
  describe('AnneeUniversitaire Management Component', () => {
    let comp: AnneeUniversitaireComponent;
    let fixture: ComponentFixture<AnneeUniversitaireComponent>;
    let service: AnneeUniversitaireService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetInscriptionTestModule],
        declarations: [AnneeUniversitaireComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc',
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc',
                })
              ),
            },
          },
        ],
      })
        .overrideTemplate(AnneeUniversitaireComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AnneeUniversitaireComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AnneeUniversitaireService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AnneeUniversitaire(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.anneeUniversitaires && comp.anneeUniversitaires[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AnneeUniversitaire(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.anneeUniversitaires && comp.anneeUniversitaires[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
