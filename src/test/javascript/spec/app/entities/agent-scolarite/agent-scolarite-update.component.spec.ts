import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetInscriptionTestModule } from '../../../test.module';
import { AgentScolariteUpdateComponent } from 'app/entities/agent-scolarite/agent-scolarite-update.component';
import { AgentScolariteService } from 'app/entities/agent-scolarite/agent-scolarite.service';
import { AgentScolarite } from 'app/shared/model/agent-scolarite.model';

describe('Component Tests', () => {
  describe('AgentScolarite Management Update Component', () => {
    let comp: AgentScolariteUpdateComponent;
    let fixture: ComponentFixture<AgentScolariteUpdateComponent>;
    let service: AgentScolariteService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetInscriptionTestModule],
        declarations: [AgentScolariteUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AgentScolariteUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentScolariteUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentScolariteService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentScolarite(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentScolarite();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
