import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjetInscriptionTestModule } from '../../../test.module';
import { AgentScolariteDetailComponent } from 'app/entities/agent-scolarite/agent-scolarite-detail.component';
import { AgentScolarite } from 'app/shared/model/agent-scolarite.model';

describe('Component Tests', () => {
  describe('AgentScolarite Management Detail Component', () => {
    let comp: AgentScolariteDetailComponent;
    let fixture: ComponentFixture<AgentScolariteDetailComponent>;
    const route = ({ data: of({ agentScolarite: new AgentScolarite(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetInscriptionTestModule],
        declarations: [AgentScolariteDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AgentScolariteDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AgentScolariteDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load agentScolarite on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.agentScolarite).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
