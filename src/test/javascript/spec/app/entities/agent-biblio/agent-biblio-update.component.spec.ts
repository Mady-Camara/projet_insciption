import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ProjetInscriptionTestModule } from '../../../test.module';
import { AgentBiblioUpdateComponent } from 'app/entities/agent-biblio/agent-biblio-update.component';
import { AgentBiblioService } from 'app/entities/agent-biblio/agent-biblio.service';
import { AgentBiblio } from 'app/shared/model/agent-biblio.model';

describe('Component Tests', () => {
  describe('AgentBiblio Management Update Component', () => {
    let comp: AgentBiblioUpdateComponent;
    let fixture: ComponentFixture<AgentBiblioUpdateComponent>;
    let service: AgentBiblioService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ProjetInscriptionTestModule],
        declarations: [AgentBiblioUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AgentBiblioUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentBiblioUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentBiblioService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentBiblio(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentBiblio();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
