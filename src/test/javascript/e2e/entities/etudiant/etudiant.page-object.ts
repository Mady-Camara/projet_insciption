import { element, by, ElementFinder } from 'protractor';

export class EtudiantComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-etudiant div table .btn-danger'));
  title = element.all(by.css('jhi-etudiant div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class EtudiantUpdatePage {
  pageTitle = element(by.id('jhi-etudiant-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  numIdentifiantInput = element(by.id('field_numIdentifiant'));
  dateNaissanceInput = element(by.id('field_dateNaissance'));
  lieuNaissanceInput = element(by.id('field_lieuNaissance'));
  sexeSelect = element(by.id('field_sexe'));
  ineInput = element(by.id('field_ine'));
  telephoneInput = element(by.id('field_telephone'));
  emailInput = element(by.id('field_email'));
  nomDuMariInput = element(by.id('field_nomDuMari'));
  regionDeNaissanceInput = element(by.id('field_regionDeNaissance'));
  prenom2Input = element(by.id('field_prenom2'));
  prenom3Input = element(by.id('field_prenom3'));
  paysDeNaissanceInput = element(by.id('field_paysDeNaissance'));
  nationaliteInput = element(by.id('field_nationalite'));
  addresseADakarInput = element(by.id('field_addresseADakar'));
  boitePostalInput = element(by.id('field_boitePostal'));
  portableInput = element(by.id('field_portable'));
  activiteSalarieeInput = element(by.id('field_activiteSalariee'));
  categorieSocioprofessionnelleInput = element(by.id('field_categorieSocioprofessionnelle'));
  situationFamilialeInput = element(by.id('field_situationFamiliale'));
  nombreEnfantInput = element(by.id('field_nombreEnfant'));
  bourseSelect = element(by.id('field_bourse'));
  natureBourseSelect = element(by.id('field_natureBourse'));
  montantBourseInput = element(by.id('field_montantBourse'));
  organismeBoursierInput = element(by.id('field_organismeBoursier'));
  serieDiplomeInput = element(by.id('field_serieDiplome'));
  anneeDiplomeInput = element(by.id('field_anneeDiplome'));
  mentionDiplomeInput = element(by.id('field_mentionDiplome'));
  lieuDiplomeInput = element(by.id('field_lieuDiplome'));
  serieDuelDuesDutBtsInput = element(by.id('field_serieDuelDuesDutBts'));
  anneeDuelDuesDutBtsInput = element(by.id('field_anneeDuelDuesDutBts'));
  mentionDuelDuesDutBtsInput = element(by.id('field_mentionDuelDuesDutBts'));
  lieuDuelDuesDutBtsInput = element(by.id('field_lieuDuelDuesDutBts'));
  serieLicenceCompleteInput = element(by.id('field_serieLicenceComplete'));
  anneeLicenceCompleteInput = element(by.id('field_anneeLicenceComplete'));
  mentionLicenceCompleteInput = element(by.id('field_mentionLicenceComplete'));
  lieuLicenceCompleteInput = element(by.id('field_lieuLicenceComplete'));
  serieMasterInput = element(by.id('field_serieMaster'));
  anneeMasterInput = element(by.id('field_anneeMaster'));
  mentionMasterInput = element(by.id('field_mentionMaster'));
  lieuInput = element(by.id('field_lieu'));
  serieDoctoratInput = element(by.id('field_serieDoctorat'));
  anneeDoctoratInput = element(by.id('field_anneeDoctorat'));
  mentionDoctoratInput = element(by.id('field_mentionDoctorat'));
  lieuDoctoratInput = element(by.id('field_lieuDoctorat'));
  nomPersonneAContacterInput = element(by.id('field_nomPersonneAContacter'));
  nomDuMariPersonneAContacterInput = element(by.id('field_nomDuMariPersonneAContacter'));
  lienDeParentePersonneAContacterInput = element(by.id('field_lienDeParentePersonneAContacter'));
  adressePersonneAContacterInput = element(by.id('field_adressePersonneAContacter'));
  rueQuartierPersonneAContacterInput = element(by.id('field_rueQuartierPersonneAContacter'));
  villePersonneAContacterInput = element(by.id('field_villePersonneAContacter'));
  telephonePersonneAContacterInput = element(by.id('field_telephonePersonneAContacter'));
  prenomPersonneAContacterInput = element(by.id('field_prenomPersonneAContacter'));
  prenom2PersonneAContacterInput = element(by.id('field_prenom2PersonneAContacter'));
  prenom3PersonneAContacterInput = element(by.id('field_prenom3PersonneAContacter'));
  boitePostalePersonneAContacterInput = element(by.id('field_boitePostalePersonneAContacter'));
  portPersonneAContacterInput = element(by.id('field_portPersonneAContacter'));
  faxPersonneAContacterInput = element(by.id('field_faxPersonneAContacter'));
  emialPersonneAContacterInput = element(by.id('field_emialPersonneAContacter'));
  responsableEstEtudiantInput = element(by.id('field_responsableEstEtudiant'));
  personneAContacterInput = element(by.id('field_personneAContacter'));
  statusEtudiantSelect = element(by.id('field_statusEtudiant'));

  userSelect = element(by.id('field_user'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNumIdentifiantInput(numIdentifiant: string): Promise<void> {
    await this.numIdentifiantInput.sendKeys(numIdentifiant);
  }

  async getNumIdentifiantInput(): Promise<string> {
    return await this.numIdentifiantInput.getAttribute('value');
  }

  async setDateNaissanceInput(dateNaissance: string): Promise<void> {
    await this.dateNaissanceInput.sendKeys(dateNaissance);
  }

  async getDateNaissanceInput(): Promise<string> {
    return await this.dateNaissanceInput.getAttribute('value');
  }

  async setLieuNaissanceInput(lieuNaissance: string): Promise<void> {
    await this.lieuNaissanceInput.sendKeys(lieuNaissance);
  }

  async getLieuNaissanceInput(): Promise<string> {
    return await this.lieuNaissanceInput.getAttribute('value');
  }

  async setSexeSelect(sexe: string): Promise<void> {
    await this.sexeSelect.sendKeys(sexe);
  }

  async getSexeSelect(): Promise<string> {
    return await this.sexeSelect.element(by.css('option:checked')).getText();
  }

  async sexeSelectLastOption(): Promise<void> {
    await this.sexeSelect.all(by.tagName('option')).last().click();
  }

  async setIneInput(ine: string): Promise<void> {
    await this.ineInput.sendKeys(ine);
  }

  async getIneInput(): Promise<string> {
    return await this.ineInput.getAttribute('value');
  }

  async setTelephoneInput(telephone: string): Promise<void> {
    await this.telephoneInput.sendKeys(telephone);
  }

  async getTelephoneInput(): Promise<string> {
    return await this.telephoneInput.getAttribute('value');
  }

  async setEmailInput(email: string): Promise<void> {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput(): Promise<string> {
    return await this.emailInput.getAttribute('value');
  }

  async setNomDuMariInput(nomDuMari: string): Promise<void> {
    await this.nomDuMariInput.sendKeys(nomDuMari);
  }

  async getNomDuMariInput(): Promise<string> {
    return await this.nomDuMariInput.getAttribute('value');
  }

  async setRegionDeNaissanceInput(regionDeNaissance: string): Promise<void> {
    await this.regionDeNaissanceInput.sendKeys(regionDeNaissance);
  }

  async getRegionDeNaissanceInput(): Promise<string> {
    return await this.regionDeNaissanceInput.getAttribute('value');
  }

  async setPrenom2Input(prenom2: string): Promise<void> {
    await this.prenom2Input.sendKeys(prenom2);
  }

  async getPrenom2Input(): Promise<string> {
    return await this.prenom2Input.getAttribute('value');
  }

  async setPrenom3Input(prenom3: string): Promise<void> {
    await this.prenom3Input.sendKeys(prenom3);
  }

  async getPrenom3Input(): Promise<string> {
    return await this.prenom3Input.getAttribute('value');
  }

  async setPaysDeNaissanceInput(paysDeNaissance: string): Promise<void> {
    await this.paysDeNaissanceInput.sendKeys(paysDeNaissance);
  }

  async getPaysDeNaissanceInput(): Promise<string> {
    return await this.paysDeNaissanceInput.getAttribute('value');
  }

  async setNationaliteInput(nationalite: string): Promise<void> {
    await this.nationaliteInput.sendKeys(nationalite);
  }

  async getNationaliteInput(): Promise<string> {
    return await this.nationaliteInput.getAttribute('value');
  }

  async setAddresseADakarInput(addresseADakar: string): Promise<void> {
    await this.addresseADakarInput.sendKeys(addresseADakar);
  }

  async getAddresseADakarInput(): Promise<string> {
    return await this.addresseADakarInput.getAttribute('value');
  }

  async setBoitePostalInput(boitePostal: string): Promise<void> {
    await this.boitePostalInput.sendKeys(boitePostal);
  }

  async getBoitePostalInput(): Promise<string> {
    return await this.boitePostalInput.getAttribute('value');
  }

  async setPortableInput(portable: string): Promise<void> {
    await this.portableInput.sendKeys(portable);
  }

  async getPortableInput(): Promise<string> {
    return await this.portableInput.getAttribute('value');
  }

  getActiviteSalarieeInput(): ElementFinder {
    return this.activiteSalarieeInput;
  }

  async setCategorieSocioprofessionnelleInput(categorieSocioprofessionnelle: string): Promise<void> {
    await this.categorieSocioprofessionnelleInput.sendKeys(categorieSocioprofessionnelle);
  }

  async getCategorieSocioprofessionnelleInput(): Promise<string> {
    return await this.categorieSocioprofessionnelleInput.getAttribute('value');
  }

  async setSituationFamilialeInput(situationFamiliale: string): Promise<void> {
    await this.situationFamilialeInput.sendKeys(situationFamiliale);
  }

  async getSituationFamilialeInput(): Promise<string> {
    return await this.situationFamilialeInput.getAttribute('value');
  }

  async setNombreEnfantInput(nombreEnfant: string): Promise<void> {
    await this.nombreEnfantInput.sendKeys(nombreEnfant);
  }

  async getNombreEnfantInput(): Promise<string> {
    return await this.nombreEnfantInput.getAttribute('value');
  }

  async setBourseSelect(bourse: string): Promise<void> {
    await this.bourseSelect.sendKeys(bourse);
  }

  async getBourseSelect(): Promise<string> {
    return await this.bourseSelect.element(by.css('option:checked')).getText();
  }

  async bourseSelectLastOption(): Promise<void> {
    await this.bourseSelect.all(by.tagName('option')).last().click();
  }

  async setNatureBourseSelect(natureBourse: string): Promise<void> {
    await this.natureBourseSelect.sendKeys(natureBourse);
  }

  async getNatureBourseSelect(): Promise<string> {
    return await this.natureBourseSelect.element(by.css('option:checked')).getText();
  }

  async natureBourseSelectLastOption(): Promise<void> {
    await this.natureBourseSelect.all(by.tagName('option')).last().click();
  }

  async setMontantBourseInput(montantBourse: string): Promise<void> {
    await this.montantBourseInput.sendKeys(montantBourse);
  }

  async getMontantBourseInput(): Promise<string> {
    return await this.montantBourseInput.getAttribute('value');
  }

  async setOrganismeBoursierInput(organismeBoursier: string): Promise<void> {
    await this.organismeBoursierInput.sendKeys(organismeBoursier);
  }

  async getOrganismeBoursierInput(): Promise<string> {
    return await this.organismeBoursierInput.getAttribute('value');
  }

  async setSerieDiplomeInput(serieDiplome: string): Promise<void> {
    await this.serieDiplomeInput.sendKeys(serieDiplome);
  }

  async getSerieDiplomeInput(): Promise<string> {
    return await this.serieDiplomeInput.getAttribute('value');
  }

  async setAnneeDiplomeInput(anneeDiplome: string): Promise<void> {
    await this.anneeDiplomeInput.sendKeys(anneeDiplome);
  }

  async getAnneeDiplomeInput(): Promise<string> {
    return await this.anneeDiplomeInput.getAttribute('value');
  }

  async setMentionDiplomeInput(mentionDiplome: string): Promise<void> {
    await this.mentionDiplomeInput.sendKeys(mentionDiplome);
  }

  async getMentionDiplomeInput(): Promise<string> {
    return await this.mentionDiplomeInput.getAttribute('value');
  }

  async setLieuDiplomeInput(lieuDiplome: string): Promise<void> {
    await this.lieuDiplomeInput.sendKeys(lieuDiplome);
  }

  async getLieuDiplomeInput(): Promise<string> {
    return await this.lieuDiplomeInput.getAttribute('value');
  }

  async setSerieDuelDuesDutBtsInput(serieDuelDuesDutBts: string): Promise<void> {
    await this.serieDuelDuesDutBtsInput.sendKeys(serieDuelDuesDutBts);
  }

  async getSerieDuelDuesDutBtsInput(): Promise<string> {
    return await this.serieDuelDuesDutBtsInput.getAttribute('value');
  }

  async setAnneeDuelDuesDutBtsInput(anneeDuelDuesDutBts: string): Promise<void> {
    await this.anneeDuelDuesDutBtsInput.sendKeys(anneeDuelDuesDutBts);
  }

  async getAnneeDuelDuesDutBtsInput(): Promise<string> {
    return await this.anneeDuelDuesDutBtsInput.getAttribute('value');
  }

  async setMentionDuelDuesDutBtsInput(mentionDuelDuesDutBts: string): Promise<void> {
    await this.mentionDuelDuesDutBtsInput.sendKeys(mentionDuelDuesDutBts);
  }

  async getMentionDuelDuesDutBtsInput(): Promise<string> {
    return await this.mentionDuelDuesDutBtsInput.getAttribute('value');
  }

  async setLieuDuelDuesDutBtsInput(lieuDuelDuesDutBts: string): Promise<void> {
    await this.lieuDuelDuesDutBtsInput.sendKeys(lieuDuelDuesDutBts);
  }

  async getLieuDuelDuesDutBtsInput(): Promise<string> {
    return await this.lieuDuelDuesDutBtsInput.getAttribute('value');
  }

  async setSerieLicenceCompleteInput(serieLicenceComplete: string): Promise<void> {
    await this.serieLicenceCompleteInput.sendKeys(serieLicenceComplete);
  }

  async getSerieLicenceCompleteInput(): Promise<string> {
    return await this.serieLicenceCompleteInput.getAttribute('value');
  }

  async setAnneeLicenceCompleteInput(anneeLicenceComplete: string): Promise<void> {
    await this.anneeLicenceCompleteInput.sendKeys(anneeLicenceComplete);
  }

  async getAnneeLicenceCompleteInput(): Promise<string> {
    return await this.anneeLicenceCompleteInput.getAttribute('value');
  }

  async setMentionLicenceCompleteInput(mentionLicenceComplete: string): Promise<void> {
    await this.mentionLicenceCompleteInput.sendKeys(mentionLicenceComplete);
  }

  async getMentionLicenceCompleteInput(): Promise<string> {
    return await this.mentionLicenceCompleteInput.getAttribute('value');
  }

  async setLieuLicenceCompleteInput(lieuLicenceComplete: string): Promise<void> {
    await this.lieuLicenceCompleteInput.sendKeys(lieuLicenceComplete);
  }

  async getLieuLicenceCompleteInput(): Promise<string> {
    return await this.lieuLicenceCompleteInput.getAttribute('value');
  }

  async setSerieMasterInput(serieMaster: string): Promise<void> {
    await this.serieMasterInput.sendKeys(serieMaster);
  }

  async getSerieMasterInput(): Promise<string> {
    return await this.serieMasterInput.getAttribute('value');
  }

  async setAnneeMasterInput(anneeMaster: string): Promise<void> {
    await this.anneeMasterInput.sendKeys(anneeMaster);
  }

  async getAnneeMasterInput(): Promise<string> {
    return await this.anneeMasterInput.getAttribute('value');
  }

  async setMentionMasterInput(mentionMaster: string): Promise<void> {
    await this.mentionMasterInput.sendKeys(mentionMaster);
  }

  async getMentionMasterInput(): Promise<string> {
    return await this.mentionMasterInput.getAttribute('value');
  }

  async setLieuInput(lieu: string): Promise<void> {
    await this.lieuInput.sendKeys(lieu);
  }

  async getLieuInput(): Promise<string> {
    return await this.lieuInput.getAttribute('value');
  }

  async setSerieDoctoratInput(serieDoctorat: string): Promise<void> {
    await this.serieDoctoratInput.sendKeys(serieDoctorat);
  }

  async getSerieDoctoratInput(): Promise<string> {
    return await this.serieDoctoratInput.getAttribute('value');
  }

  async setAnneeDoctoratInput(anneeDoctorat: string): Promise<void> {
    await this.anneeDoctoratInput.sendKeys(anneeDoctorat);
  }

  async getAnneeDoctoratInput(): Promise<string> {
    return await this.anneeDoctoratInput.getAttribute('value');
  }

  async setMentionDoctoratInput(mentionDoctorat: string): Promise<void> {
    await this.mentionDoctoratInput.sendKeys(mentionDoctorat);
  }

  async getMentionDoctoratInput(): Promise<string> {
    return await this.mentionDoctoratInput.getAttribute('value');
  }

  async setLieuDoctoratInput(lieuDoctorat: string): Promise<void> {
    await this.lieuDoctoratInput.sendKeys(lieuDoctorat);
  }

  async getLieuDoctoratInput(): Promise<string> {
    return await this.lieuDoctoratInput.getAttribute('value');
  }

  async setNomPersonneAContacterInput(nomPersonneAContacter: string): Promise<void> {
    await this.nomPersonneAContacterInput.sendKeys(nomPersonneAContacter);
  }

  async getNomPersonneAContacterInput(): Promise<string> {
    return await this.nomPersonneAContacterInput.getAttribute('value');
  }

  async setNomDuMariPersonneAContacterInput(nomDuMariPersonneAContacter: string): Promise<void> {
    await this.nomDuMariPersonneAContacterInput.sendKeys(nomDuMariPersonneAContacter);
  }

  async getNomDuMariPersonneAContacterInput(): Promise<string> {
    return await this.nomDuMariPersonneAContacterInput.getAttribute('value');
  }

  async setLienDeParentePersonneAContacterInput(lienDeParentePersonneAContacter: string): Promise<void> {
    await this.lienDeParentePersonneAContacterInput.sendKeys(lienDeParentePersonneAContacter);
  }

  async getLienDeParentePersonneAContacterInput(): Promise<string> {
    return await this.lienDeParentePersonneAContacterInput.getAttribute('value');
  }

  async setAdressePersonneAContacterInput(adressePersonneAContacter: string): Promise<void> {
    await this.adressePersonneAContacterInput.sendKeys(adressePersonneAContacter);
  }

  async getAdressePersonneAContacterInput(): Promise<string> {
    return await this.adressePersonneAContacterInput.getAttribute('value');
  }

  async setRueQuartierPersonneAContacterInput(rueQuartierPersonneAContacter: string): Promise<void> {
    await this.rueQuartierPersonneAContacterInput.sendKeys(rueQuartierPersonneAContacter);
  }

  async getRueQuartierPersonneAContacterInput(): Promise<string> {
    return await this.rueQuartierPersonneAContacterInput.getAttribute('value');
  }

  async setVillePersonneAContacterInput(villePersonneAContacter: string): Promise<void> {
    await this.villePersonneAContacterInput.sendKeys(villePersonneAContacter);
  }

  async getVillePersonneAContacterInput(): Promise<string> {
    return await this.villePersonneAContacterInput.getAttribute('value');
  }

  async setTelephonePersonneAContacterInput(telephonePersonneAContacter: string): Promise<void> {
    await this.telephonePersonneAContacterInput.sendKeys(telephonePersonneAContacter);
  }

  async getTelephonePersonneAContacterInput(): Promise<string> {
    return await this.telephonePersonneAContacterInput.getAttribute('value');
  }

  async setPrenomPersonneAContacterInput(prenomPersonneAContacter: string): Promise<void> {
    await this.prenomPersonneAContacterInput.sendKeys(prenomPersonneAContacter);
  }

  async getPrenomPersonneAContacterInput(): Promise<string> {
    return await this.prenomPersonneAContacterInput.getAttribute('value');
  }

  async setPrenom2PersonneAContacterInput(prenom2PersonneAContacter: string): Promise<void> {
    await this.prenom2PersonneAContacterInput.sendKeys(prenom2PersonneAContacter);
  }

  async getPrenom2PersonneAContacterInput(): Promise<string> {
    return await this.prenom2PersonneAContacterInput.getAttribute('value');
  }

  async setPrenom3PersonneAContacterInput(prenom3PersonneAContacter: string): Promise<void> {
    await this.prenom3PersonneAContacterInput.sendKeys(prenom3PersonneAContacter);
  }

  async getPrenom3PersonneAContacterInput(): Promise<string> {
    return await this.prenom3PersonneAContacterInput.getAttribute('value');
  }

  async setBoitePostalePersonneAContacterInput(boitePostalePersonneAContacter: string): Promise<void> {
    await this.boitePostalePersonneAContacterInput.sendKeys(boitePostalePersonneAContacter);
  }

  async getBoitePostalePersonneAContacterInput(): Promise<string> {
    return await this.boitePostalePersonneAContacterInput.getAttribute('value');
  }

  async setPortPersonneAContacterInput(portPersonneAContacter: string): Promise<void> {
    await this.portPersonneAContacterInput.sendKeys(portPersonneAContacter);
  }

  async getPortPersonneAContacterInput(): Promise<string> {
    return await this.portPersonneAContacterInput.getAttribute('value');
  }

  async setFaxPersonneAContacterInput(faxPersonneAContacter: string): Promise<void> {
    await this.faxPersonneAContacterInput.sendKeys(faxPersonneAContacter);
  }

  async getFaxPersonneAContacterInput(): Promise<string> {
    return await this.faxPersonneAContacterInput.getAttribute('value');
  }

  async setEmialPersonneAContacterInput(emialPersonneAContacter: string): Promise<void> {
    await this.emialPersonneAContacterInput.sendKeys(emialPersonneAContacter);
  }

  async getEmialPersonneAContacterInput(): Promise<string> {
    return await this.emialPersonneAContacterInput.getAttribute('value');
  }

  getResponsableEstEtudiantInput(): ElementFinder {
    return this.responsableEstEtudiantInput;
  }

  getPersonneAContacterInput(): ElementFinder {
    return this.personneAContacterInput;
  }

  async setStatusEtudiantSelect(statusEtudiant: string): Promise<void> {
    await this.statusEtudiantSelect.sendKeys(statusEtudiant);
  }

  async getStatusEtudiantSelect(): Promise<string> {
    return await this.statusEtudiantSelect.element(by.css('option:checked')).getText();
  }

  async statusEtudiantSelectLastOption(): Promise<void> {
    await this.statusEtudiantSelect.all(by.tagName('option')).last().click();
  }

  async userSelectLastOption(): Promise<void> {
    await this.userSelect.all(by.tagName('option')).last().click();
  }

  async userSelectOption(option: string): Promise<void> {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect(): ElementFinder {
    return this.userSelect;
  }

  async getUserSelectedOption(): Promise<string> {
    return await this.userSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EtudiantDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-etudiant-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-etudiant'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
