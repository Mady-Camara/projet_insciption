import { element, by, ElementFinder } from 'protractor';

export class InscriptionComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-inscription div table .btn-danger'));
  title = element.all(by.css('jhi-inscription div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class InscriptionUpdatePage {
  pageTitle = element(by.id('jhi-inscription-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  estApteInput = element(by.id('field_estApte'));
  estBoursierInput = element(by.id('field_estBoursier'));
  estAssureInput = element(by.id('field_estAssure'));
  enRegleBiblioInput = element(by.id('field_enRegleBiblio'));
  situationMatrimonialeSelect = element(by.id('field_situationMatrimoniale'));
  anneeEtudeInput = element(by.id('field_anneeEtude'));
  cycleInput = element(by.id('field_cycle'));
  departementInput = element(by.id('field_departement'));
  optionChoisieInput = element(by.id('field_optionChoisie'));
  nombreInscriptionAnterieurSelect = element(by.id('field_nombreInscriptionAnterieur'));
  redoublezVousInput = element(by.id('field_redoublezVous'));
  horaireDesTdSelect = element(by.id('field_horaireDesTd'));

  niveauSelect = element(by.id('field_niveau'));
  anneeUniversitaireSelect = element(by.id('field_anneeUniversitaire'));
  etudiantSelect = element(by.id('field_etudiant'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  getEstApteInput(): ElementFinder {
    return this.estApteInput;
  }

  getEstBoursierInput(): ElementFinder {
    return this.estBoursierInput;
  }

  getEstAssureInput(): ElementFinder {
    return this.estAssureInput;
  }

  getEnRegleBiblioInput(): ElementFinder {
    return this.enRegleBiblioInput;
  }

  async setSituationMatrimonialeSelect(situationMatrimoniale: string): Promise<void> {
    await this.situationMatrimonialeSelect.sendKeys(situationMatrimoniale);
  }

  async getSituationMatrimonialeSelect(): Promise<string> {
    return await this.situationMatrimonialeSelect.element(by.css('option:checked')).getText();
  }

  async situationMatrimonialeSelectLastOption(): Promise<void> {
    await this.situationMatrimonialeSelect.all(by.tagName('option')).last().click();
  }

  async setAnneeEtudeInput(anneeEtude: string): Promise<void> {
    await this.anneeEtudeInput.sendKeys(anneeEtude);
  }

  async getAnneeEtudeInput(): Promise<string> {
    return await this.anneeEtudeInput.getAttribute('value');
  }

  async setCycleInput(cycle: string): Promise<void> {
    await this.cycleInput.sendKeys(cycle);
  }

  async getCycleInput(): Promise<string> {
    return await this.cycleInput.getAttribute('value');
  }

  async setDepartementInput(departement: string): Promise<void> {
    await this.departementInput.sendKeys(departement);
  }

  async getDepartementInput(): Promise<string> {
    return await this.departementInput.getAttribute('value');
  }

  async setOptionChoisieInput(optionChoisie: string): Promise<void> {
    await this.optionChoisieInput.sendKeys(optionChoisie);
  }

  async getOptionChoisieInput(): Promise<string> {
    return await this.optionChoisieInput.getAttribute('value');
  }

  async setNombreInscriptionAnterieurSelect(nombreInscriptionAnterieur: string): Promise<void> {
    await this.nombreInscriptionAnterieurSelect.sendKeys(nombreInscriptionAnterieur);
  }

  async getNombreInscriptionAnterieurSelect(): Promise<string> {
    return await this.nombreInscriptionAnterieurSelect.element(by.css('option:checked')).getText();
  }

  async nombreInscriptionAnterieurSelectLastOption(): Promise<void> {
    await this.nombreInscriptionAnterieurSelect.all(by.tagName('option')).last().click();
  }

  getRedoublezVousInput(): ElementFinder {
    return this.redoublezVousInput;
  }

  async setHoraireDesTdSelect(horaireDesTd: string): Promise<void> {
    await this.horaireDesTdSelect.sendKeys(horaireDesTd);
  }

  async getHoraireDesTdSelect(): Promise<string> {
    return await this.horaireDesTdSelect.element(by.css('option:checked')).getText();
  }

  async horaireDesTdSelectLastOption(): Promise<void> {
    await this.horaireDesTdSelect.all(by.tagName('option')).last().click();
  }

  async niveauSelectLastOption(): Promise<void> {
    await this.niveauSelect.all(by.tagName('option')).last().click();
  }

  async niveauSelectOption(option: string): Promise<void> {
    await this.niveauSelect.sendKeys(option);
  }

  getNiveauSelect(): ElementFinder {
    return this.niveauSelect;
  }

  async getNiveauSelectedOption(): Promise<string> {
    return await this.niveauSelect.element(by.css('option:checked')).getText();
  }

  async anneeUniversitaireSelectLastOption(): Promise<void> {
    await this.anneeUniversitaireSelect.all(by.tagName('option')).last().click();
  }

  async anneeUniversitaireSelectOption(option: string): Promise<void> {
    await this.anneeUniversitaireSelect.sendKeys(option);
  }

  getAnneeUniversitaireSelect(): ElementFinder {
    return this.anneeUniversitaireSelect;
  }

  async getAnneeUniversitaireSelectedOption(): Promise<string> {
    return await this.anneeUniversitaireSelect.element(by.css('option:checked')).getText();
  }

  async etudiantSelectLastOption(): Promise<void> {
    await this.etudiantSelect.all(by.tagName('option')).last().click();
  }

  async etudiantSelectOption(option: string): Promise<void> {
    await this.etudiantSelect.sendKeys(option);
  }

  getEtudiantSelect(): ElementFinder {
    return this.etudiantSelect;
  }

  async getEtudiantSelectedOption(): Promise<string> {
    return await this.etudiantSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class InscriptionDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-inscription-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-inscription'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
