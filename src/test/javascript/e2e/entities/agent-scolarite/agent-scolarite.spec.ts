import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AgentScolariteComponentsPage, AgentScolariteDeleteDialog, AgentScolariteUpdatePage } from './agent-scolarite.page-object';

const expect = chai.expect;

describe('AgentScolarite e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let agentScolariteComponentsPage: AgentScolariteComponentsPage;
  let agentScolariteUpdatePage: AgentScolariteUpdatePage;
  let agentScolariteDeleteDialog: AgentScolariteDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AgentScolarites', async () => {
    await navBarPage.goToEntity('agent-scolarite');
    agentScolariteComponentsPage = new AgentScolariteComponentsPage();
    await browser.wait(ec.visibilityOf(agentScolariteComponentsPage.title), 5000);
    expect(await agentScolariteComponentsPage.getTitle()).to.eq('projetInscriptionApp.agentScolarite.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(agentScolariteComponentsPage.entities), ec.visibilityOf(agentScolariteComponentsPage.noResult)),
      1000
    );
  });

  it('should load create AgentScolarite page', async () => {
    await agentScolariteComponentsPage.clickOnCreateButton();
    agentScolariteUpdatePage = new AgentScolariteUpdatePage();
    expect(await agentScolariteUpdatePage.getPageTitle()).to.eq('projetInscriptionApp.agentScolarite.home.createOrEditLabel');
    await agentScolariteUpdatePage.cancel();
  });

  it('should create and save AgentScolarites', async () => {
    const nbButtonsBeforeCreate = await agentScolariteComponentsPage.countDeleteButtons();

    await agentScolariteComponentsPage.clickOnCreateButton();

    await promise.all([agentScolariteUpdatePage.setMatriculeInput('matricule'), agentScolariteUpdatePage.userSelectLastOption()]);

    expect(await agentScolariteUpdatePage.getMatriculeInput()).to.eq('matricule', 'Expected Matricule value to be equals to matricule');

    await agentScolariteUpdatePage.save();
    expect(await agentScolariteUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await agentScolariteComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last AgentScolarite', async () => {
    const nbButtonsBeforeDelete = await agentScolariteComponentsPage.countDeleteButtons();
    await agentScolariteComponentsPage.clickOnLastDeleteButton();

    agentScolariteDeleteDialog = new AgentScolariteDeleteDialog();
    expect(await agentScolariteDeleteDialog.getDialogTitle()).to.eq('projetInscriptionApp.agentScolarite.delete.question');
    await agentScolariteDeleteDialog.clickOnConfirmButton();

    expect(await agentScolariteComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
