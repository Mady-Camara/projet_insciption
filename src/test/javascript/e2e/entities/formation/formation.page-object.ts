import { element, by, ElementFinder } from 'protractor';

export class FormationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-formation div table .btn-danger'));
  title = element.all(by.css('jhi-formation div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FormationUpdatePage {
  pageTitle = element(by.id('jhi-formation-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  codeFormationInput = element(by.id('field_codeFormation'));
  libelleLongInput = element(by.id('field_libelleLong'));

  departementSelect = element(by.id('field_departement'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCodeFormationInput(codeFormation: string): Promise<void> {
    await this.codeFormationInput.sendKeys(codeFormation);
  }

  async getCodeFormationInput(): Promise<string> {
    return await this.codeFormationInput.getAttribute('value');
  }

  async setLibelleLongInput(libelleLong: string): Promise<void> {
    await this.libelleLongInput.sendKeys(libelleLong);
  }

  async getLibelleLongInput(): Promise<string> {
    return await this.libelleLongInput.getAttribute('value');
  }

  async departementSelectLastOption(): Promise<void> {
    await this.departementSelect.all(by.tagName('option')).last().click();
  }

  async departementSelectOption(option: string): Promise<void> {
    await this.departementSelect.sendKeys(option);
  }

  getDepartementSelect(): ElementFinder {
    return this.departementSelect;
  }

  async getDepartementSelectedOption(): Promise<string> {
    return await this.departementSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FormationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-formation-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-formation'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
