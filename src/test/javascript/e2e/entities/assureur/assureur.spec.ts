import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AssureurComponentsPage, AssureurDeleteDialog, AssureurUpdatePage } from './assureur.page-object';

const expect = chai.expect;

describe('Assureur e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let assureurComponentsPage: AssureurComponentsPage;
  let assureurUpdatePage: AssureurUpdatePage;
  let assureurDeleteDialog: AssureurDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Assureurs', async () => {
    await navBarPage.goToEntity('assureur');
    assureurComponentsPage = new AssureurComponentsPage();
    await browser.wait(ec.visibilityOf(assureurComponentsPage.title), 5000);
    expect(await assureurComponentsPage.getTitle()).to.eq('projetInscriptionApp.assureur.home.title');
    await browser.wait(ec.or(ec.visibilityOf(assureurComponentsPage.entities), ec.visibilityOf(assureurComponentsPage.noResult)), 1000);
  });

  it('should load create Assureur page', async () => {
    await assureurComponentsPage.clickOnCreateButton();
    assureurUpdatePage = new AssureurUpdatePage();
    expect(await assureurUpdatePage.getPageTitle()).to.eq('projetInscriptionApp.assureur.home.createOrEditLabel');
    await assureurUpdatePage.cancel();
  });

  it('should create and save Assureurs', async () => {
    const nbButtonsBeforeCreate = await assureurComponentsPage.countDeleteButtons();

    await assureurComponentsPage.clickOnCreateButton();

    await promise.all([
      assureurUpdatePage.setMatriculeInput('matricule'),
      assureurUpdatePage.setEmailInput('email'),
      assureurUpdatePage.setPasswordInput('password'),
      assureurUpdatePage.userSelectLastOption(),
    ]);

    expect(await assureurUpdatePage.getMatriculeInput()).to.eq('matricule', 'Expected Matricule value to be equals to matricule');
    expect(await assureurUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
    expect(await assureurUpdatePage.getPasswordInput()).to.eq('password', 'Expected Password value to be equals to password');

    await assureurUpdatePage.save();
    expect(await assureurUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await assureurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Assureur', async () => {
    const nbButtonsBeforeDelete = await assureurComponentsPage.countDeleteButtons();
    await assureurComponentsPage.clickOnLastDeleteButton();

    assureurDeleteDialog = new AssureurDeleteDialog();
    expect(await assureurDeleteDialog.getDialogTitle()).to.eq('projetInscriptionApp.assureur.delete.question');
    await assureurDeleteDialog.clickOnConfirmButton();

    expect(await assureurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
