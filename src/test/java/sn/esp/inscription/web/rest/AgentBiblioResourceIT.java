package sn.esp.inscription.web.rest;

import sn.esp.inscription.ProjetInscriptionApp;
import sn.esp.inscription.domain.AgentBiblio;
import sn.esp.inscription.repository.AgentBiblioRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AgentBiblioResource} REST controller.
 */
@SpringBootTest(classes = ProjetInscriptionApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AgentBiblioResourceIT {

    private static final String DEFAULT_MATRICULE = "AAAAAAAAAA";
    private static final String UPDATED_MATRICULE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    @Autowired
    private AgentBiblioRepository agentBiblioRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAgentBiblioMockMvc;

    private AgentBiblio agentBiblio;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentBiblio createEntity(EntityManager em) {
        AgentBiblio agentBiblio = new AgentBiblio()
            .matricule(DEFAULT_MATRICULE)
            .email(DEFAULT_EMAIL)
            .password(DEFAULT_PASSWORD);
        return agentBiblio;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentBiblio createUpdatedEntity(EntityManager em) {
        AgentBiblio agentBiblio = new AgentBiblio()
            .matricule(UPDATED_MATRICULE)
            .email(UPDATED_EMAIL)
            .password(UPDATED_PASSWORD);
        return agentBiblio;
    }

    @BeforeEach
    public void initTest() {
        agentBiblio = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgentBiblio() throws Exception {
        int databaseSizeBeforeCreate = agentBiblioRepository.findAll().size();
        // Create the AgentBiblio
        restAgentBiblioMockMvc.perform(post("/api/agent-biblios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentBiblio)))
            .andExpect(status().isCreated());

        // Validate the AgentBiblio in the database
        List<AgentBiblio> agentBiblioList = agentBiblioRepository.findAll();
        assertThat(agentBiblioList).hasSize(databaseSizeBeforeCreate + 1);
        AgentBiblio testAgentBiblio = agentBiblioList.get(agentBiblioList.size() - 1);
        assertThat(testAgentBiblio.getMatricule()).isEqualTo(DEFAULT_MATRICULE);
        assertThat(testAgentBiblio.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAgentBiblio.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void createAgentBiblioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agentBiblioRepository.findAll().size();

        // Create the AgentBiblio with an existing ID
        agentBiblio.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgentBiblioMockMvc.perform(post("/api/agent-biblios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentBiblio)))
            .andExpect(status().isBadRequest());

        // Validate the AgentBiblio in the database
        List<AgentBiblio> agentBiblioList = agentBiblioRepository.findAll();
        assertThat(agentBiblioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMatriculeIsRequired() throws Exception {
        int databaseSizeBeforeTest = agentBiblioRepository.findAll().size();
        // set the field null
        agentBiblio.setMatricule(null);

        // Create the AgentBiblio, which fails.


        restAgentBiblioMockMvc.perform(post("/api/agent-biblios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentBiblio)))
            .andExpect(status().isBadRequest());

        List<AgentBiblio> agentBiblioList = agentBiblioRepository.findAll();
        assertThat(agentBiblioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = agentBiblioRepository.findAll().size();
        // set the field null
        agentBiblio.setEmail(null);

        // Create the AgentBiblio, which fails.


        restAgentBiblioMockMvc.perform(post("/api/agent-biblios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentBiblio)))
            .andExpect(status().isBadRequest());

        List<AgentBiblio> agentBiblioList = agentBiblioRepository.findAll();
        assertThat(agentBiblioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = agentBiblioRepository.findAll().size();
        // set the field null
        agentBiblio.setPassword(null);

        // Create the AgentBiblio, which fails.


        restAgentBiblioMockMvc.perform(post("/api/agent-biblios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentBiblio)))
            .andExpect(status().isBadRequest());

        List<AgentBiblio> agentBiblioList = agentBiblioRepository.findAll();
        assertThat(agentBiblioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAgentBiblios() throws Exception {
        // Initialize the database
        agentBiblioRepository.saveAndFlush(agentBiblio);

        // Get all the agentBiblioList
        restAgentBiblioMockMvc.perform(get("/api/agent-biblios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agentBiblio.getId().intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)));
    }
    
    @Test
    @Transactional
    public void getAgentBiblio() throws Exception {
        // Initialize the database
        agentBiblioRepository.saveAndFlush(agentBiblio);

        // Get the agentBiblio
        restAgentBiblioMockMvc.perform(get("/api/agent-biblios/{id}", agentBiblio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(agentBiblio.getId().intValue()))
            .andExpect(jsonPath("$.matricule").value(DEFAULT_MATRICULE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD));
    }
    @Test
    @Transactional
    public void getNonExistingAgentBiblio() throws Exception {
        // Get the agentBiblio
        restAgentBiblioMockMvc.perform(get("/api/agent-biblios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgentBiblio() throws Exception {
        // Initialize the database
        agentBiblioRepository.saveAndFlush(agentBiblio);

        int databaseSizeBeforeUpdate = agentBiblioRepository.findAll().size();

        // Update the agentBiblio
        AgentBiblio updatedAgentBiblio = agentBiblioRepository.findById(agentBiblio.getId()).get();
        // Disconnect from session so that the updates on updatedAgentBiblio are not directly saved in db
        em.detach(updatedAgentBiblio);
        updatedAgentBiblio
            .matricule(UPDATED_MATRICULE)
            .email(UPDATED_EMAIL)
            .password(UPDATED_PASSWORD);

        restAgentBiblioMockMvc.perform(put("/api/agent-biblios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAgentBiblio)))
            .andExpect(status().isOk());

        // Validate the AgentBiblio in the database
        List<AgentBiblio> agentBiblioList = agentBiblioRepository.findAll();
        assertThat(agentBiblioList).hasSize(databaseSizeBeforeUpdate);
        AgentBiblio testAgentBiblio = agentBiblioList.get(agentBiblioList.size() - 1);
        assertThat(testAgentBiblio.getMatricule()).isEqualTo(UPDATED_MATRICULE);
        assertThat(testAgentBiblio.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAgentBiblio.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void updateNonExistingAgentBiblio() throws Exception {
        int databaseSizeBeforeUpdate = agentBiblioRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgentBiblioMockMvc.perform(put("/api/agent-biblios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentBiblio)))
            .andExpect(status().isBadRequest());

        // Validate the AgentBiblio in the database
        List<AgentBiblio> agentBiblioList = agentBiblioRepository.findAll();
        assertThat(agentBiblioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAgentBiblio() throws Exception {
        // Initialize the database
        agentBiblioRepository.saveAndFlush(agentBiblio);

        int databaseSizeBeforeDelete = agentBiblioRepository.findAll().size();

        // Delete the agentBiblio
        restAgentBiblioMockMvc.perform(delete("/api/agent-biblios/{id}", agentBiblio.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AgentBiblio> agentBiblioList = agentBiblioRepository.findAll();
        assertThat(agentBiblioList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
