package sn.esp.inscription.web.rest;

import sn.esp.inscription.ProjetInscriptionApp;
import sn.esp.inscription.domain.Assureur;
import sn.esp.inscription.repository.AssureurRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AssureurResource} REST controller.
 */
@SpringBootTest(classes = ProjetInscriptionApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AssureurResourceIT {

    private static final String DEFAULT_MATRICULE = "AAAAAAAAAA";
    private static final String UPDATED_MATRICULE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    @Autowired
    private AssureurRepository assureurRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssureurMockMvc;

    private Assureur assureur;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Assureur createEntity(EntityManager em) {
        Assureur assureur = new Assureur()
            .matricule(DEFAULT_MATRICULE)
            .email(DEFAULT_EMAIL)
            .password(DEFAULT_PASSWORD);
        return assureur;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Assureur createUpdatedEntity(EntityManager em) {
        Assureur assureur = new Assureur()
            .matricule(UPDATED_MATRICULE)
            .email(UPDATED_EMAIL)
            .password(UPDATED_PASSWORD);
        return assureur;
    }

    @BeforeEach
    public void initTest() {
        assureur = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssureur() throws Exception {
        int databaseSizeBeforeCreate = assureurRepository.findAll().size();
        // Create the Assureur
        restAssureurMockMvc.perform(post("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isCreated());

        // Validate the Assureur in the database
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeCreate + 1);
        Assureur testAssureur = assureurList.get(assureurList.size() - 1);
        assertThat(testAssureur.getMatricule()).isEqualTo(DEFAULT_MATRICULE);
        assertThat(testAssureur.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAssureur.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void createAssureurWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = assureurRepository.findAll().size();

        // Create the Assureur with an existing ID
        assureur.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssureurMockMvc.perform(post("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isBadRequest());

        // Validate the Assureur in the database
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMatriculeIsRequired() throws Exception {
        int databaseSizeBeforeTest = assureurRepository.findAll().size();
        // set the field null
        assureur.setMatricule(null);

        // Create the Assureur, which fails.


        restAssureurMockMvc.perform(post("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isBadRequest());

        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = assureurRepository.findAll().size();
        // set the field null
        assureur.setEmail(null);

        // Create the Assureur, which fails.


        restAssureurMockMvc.perform(post("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isBadRequest());

        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = assureurRepository.findAll().size();
        // set the field null
        assureur.setPassword(null);

        // Create the Assureur, which fails.


        restAssureurMockMvc.perform(post("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isBadRequest());

        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAssureurs() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        // Get all the assureurList
        restAssureurMockMvc.perform(get("/api/assureurs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assureur.getId().intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)));
    }
    
    @Test
    @Transactional
    public void getAssureur() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        // Get the assureur
        restAssureurMockMvc.perform(get("/api/assureurs/{id}", assureur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(assureur.getId().intValue()))
            .andExpect(jsonPath("$.matricule").value(DEFAULT_MATRICULE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD));
    }
    @Test
    @Transactional
    public void getNonExistingAssureur() throws Exception {
        // Get the assureur
        restAssureurMockMvc.perform(get("/api/assureurs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssureur() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        int databaseSizeBeforeUpdate = assureurRepository.findAll().size();

        // Update the assureur
        Assureur updatedAssureur = assureurRepository.findById(assureur.getId()).get();
        // Disconnect from session so that the updates on updatedAssureur are not directly saved in db
        em.detach(updatedAssureur);
        updatedAssureur
            .matricule(UPDATED_MATRICULE)
            .email(UPDATED_EMAIL)
            .password(UPDATED_PASSWORD);

        restAssureurMockMvc.perform(put("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAssureur)))
            .andExpect(status().isOk());

        // Validate the Assureur in the database
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeUpdate);
        Assureur testAssureur = assureurList.get(assureurList.size() - 1);
        assertThat(testAssureur.getMatricule()).isEqualTo(UPDATED_MATRICULE);
        assertThat(testAssureur.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAssureur.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void updateNonExistingAssureur() throws Exception {
        int databaseSizeBeforeUpdate = assureurRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssureurMockMvc.perform(put("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isBadRequest());

        // Validate the Assureur in the database
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAssureur() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        int databaseSizeBeforeDelete = assureurRepository.findAll().size();

        // Delete the assureur
        restAssureurMockMvc.perform(delete("/api/assureurs/{id}", assureur.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
