package sn.esp.inscription.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.esp.inscription.web.rest.TestUtil;

public class AnneeUniversitaireTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AnneeUniversitaire.class);
        AnneeUniversitaire anneeUniversitaire1 = new AnneeUniversitaire();
        anneeUniversitaire1.setId(1L);
        AnneeUniversitaire anneeUniversitaire2 = new AnneeUniversitaire();
        anneeUniversitaire2.setId(anneeUniversitaire1.getId());
        assertThat(anneeUniversitaire1).isEqualTo(anneeUniversitaire2);
        anneeUniversitaire2.setId(2L);
        assertThat(anneeUniversitaire1).isNotEqualTo(anneeUniversitaire2);
        anneeUniversitaire1.setId(null);
        assertThat(anneeUniversitaire1).isNotEqualTo(anneeUniversitaire2);
    }
}
