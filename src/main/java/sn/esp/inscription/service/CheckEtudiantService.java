package sn.esp.inscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.esp.inscription.domain.Etudiant;
import sn.esp.inscription.repository.EtudiantRepository;

@Service
public class CheckEtudiantService {
    @Autowired
    EtudiantRepository etudiantRepository;

    public Etudiant CheckValidity(String identifiant){
        Etudiant etudiant = new Etudiant();
        etudiant = etudiantRepository.findByNumIdentifiant(identifiant);
            return etudiant;
    }

}
