package sn.esp.inscription.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import sn.esp.inscription.domain.Etudiant;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.repository.EtudiantRepository;
import sn.esp.inscription.repository.InscriptionRepository;

import java.util.List;

@Service
public class EtudiantService {
    @Autowired
    EtudiantRepository etudiantRepository;

    @Autowired
    InscriptionRepository inscriptionRepository;

    public List<Etudiant> findAll(){
        return etudiantRepository.findAll();
    }

    public long count(){
        return etudiantRepository.count();
    }

    public void delete(Etudiant etudiant){
        etudiantRepository.delete(etudiant);
    }

    public Etudiant checkEtudiant(Etudiant etudiant) throws NotFoundException {
        Etudiant find = etudiantRepository.findByNumIdentifiantOrTelephone(etudiant.getNumIdentifiant(), etudiant.getTelephone());
        if(find == null){
            throw new NotFoundException("Cet etudiant n'existe pas");
        }
        return find;
    }

    public Etudiant getEtudiantInfo(@PathVariable String num){
        Etudiant etudiant = etudiantRepository.findByNumIdentifiant(num);
        return etudiant;
    }

    public Inscription InscritionActive(String num){
        Etudiant result = etudiantRepository.findByNumIdentifiantIgnoreCase(num);
        List<Inscription> inscriptions = inscriptionRepository.findByEtudiant(result);
        Inscription inscription = new Inscription();
        for(Inscription trouve : inscriptions){
            if(trouve.getAnneeUniversitaire().isIsActive()){
                 inscription = trouve;
                return inscription;
            }
        }
        return inscription;
    }


}
