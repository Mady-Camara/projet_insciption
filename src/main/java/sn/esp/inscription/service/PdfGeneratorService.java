package sn.esp.inscription.service;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

@Service
public class PdfGeneratorService {

    public static Connection getMySQLConnection()
        throws ClassNotFoundException, SQLException {
        String hostName = "localhost";
        String dbName = "PUBLIC";
        String userName = "projet_inscription";
        String password = "";
        return getMySQLConnection(hostName, dbName, userName, password);
    }

    public static Connection getMySQLConnection(String hostName, String dbName, String userName, String password) throws SQLException,
        ClassNotFoundException {

        // Declare the class Driver for MySQL DB
        // This is necessary with Java 5 (or older)
        // Java6 (or newer) automatically find the appropriate driver.
        // If you use Java> 5, then this line is not needed.
        Class.forName("org.h2.Driver");

        String connectionURL = "jdbc:h2:file:./target/h2db/db/projet_inscription" + hostName + ":18080/" + dbName+"projet_inscription";

        Connection conn = DriverManager.getConnection(connectionURL, userName, password);
        return conn;

    }

    public static void generateReport (String format,String fileSource,String fileDestination ,String mainReportRequest, Map<String,Object> mainReportParams,Map<String,String> subReportMap,Map<String,String> subReportRequest) throws JRException, ClassNotFoundException, SQLException, JRException {

        //on ajoute les 2 sous rapports
        for(String key : subReportMap.keySet()) {
            putSubReport(subReportMap, key, mainReportParams, subReportRequest);
        }

        JasperDesign jasperDesign = JRXmlLoader.load(fileSource);

        if(mainReportRequest !=null) {
            JRDesignQuery jrDesignQuery = new JRDesignQuery();
            jrDesignQuery.setText(mainReportRequest);
            jasperDesign.setQuery(jrDesignQuery);
        }


        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, mainReportParams, getMySQLConnection());
        switch(format) {
            case "pdf":
                JasperExportManager.exportReportToPdfFile(jasperPrint,"jasper_generated_pdf/" + fileDestination + ".pdf");
                break;
            case "html":
                JasperExportManager.exportReportToHtmlFile(jasperPrint,"jasper_generated_html/" + fileDestination + ".html");
                break;
            default:
                JasperExportManager.exportReportToPdfFile(jasperPrint,"jasper_generated_pdf/" + fileDestination + ".pdf");
                break;
        }
        JasperExportManager.exportReportToHtmlFile(jasperPrint,fileDestination);
        System.out.println(fileDestination+"."+format+" was successfully export!! ");
    }



    public static void generateReport (String format,String fileSource,String fileDestination ,String request, Map<String,Object> parameters) throws JRException, ClassNotFoundException, SQLException {

        JasperDesign jasperDesign = JRXmlLoader.load(fileSource);

        if(request !=null) {
            JRDesignQuery jrDesignQuery = new JRDesignQuery();
            jrDesignQuery.setText(request);
            jasperDesign.setQuery(jrDesignQuery);
        }

        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, getMySQLConnection());

        switch(format) {
            case "pdf":
                JasperExportManager.exportReportToPdfFile(jasperPrint,"jasper_generated_pdf/" + fileDestination + ".pdf");
                break;
            case "html":
                JasperExportManager.exportReportToHtmlFile(jasperPrint,"jasper_generated_html/" + fileDestination + ".html");
                break;
            default:
                System.out.println("Format not defined");
                break;
        }
        System.out.println(fileDestination+"."+format+" was successfully export!! ");
    }



    public static void putSubReport(Map<String,String> subReportMap, String subReportMapKey, Map <String,Object> mainReportParams, Map<String,String> subReportRequest) throws JRException {

        // On charge un rapport à partir de la map
        JasperDesign jasperDesign = JRXmlLoader.load(subReportMap.get(subReportMapKey));

        if(subReportRequest !=null) {
            JRDesignQuery designQuery = new JRDesignQuery();
            designQuery.setText(subReportRequest.get(subReportMapKey));
            jasperDesign.setQuery(designQuery);
        }
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        // on ajoute le sous rapport comme paramètre du rapport principal
        mainReportParams.put(subReportMapKey, jasperReport);
    }
}
