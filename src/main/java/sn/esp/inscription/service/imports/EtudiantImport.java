package sn.esp.inscription.service.imports;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sn.esp.inscription.domain.Etudiant;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.domain.Niveau;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.repository.EtudiantRepository;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.repository.NiveauRepository;
import sn.esp.inscription.repository.UserRepository;

import java.io.IOException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Service
public class EtudiantImport {

    @Autowired
    EtudiantRepository etudiantRepository;

    @Autowired
    NiveauRepository niveauRepository;

    @Autowired
    InscriptionRepository inscriptionRepository;

    @Autowired
    UserRepository userRepository;

    public List<Etudiant> getEtudiant(final MultipartFile multipart) throws IOException {
        List<Etudiant> EtudiantList = new ArrayList<>();
        List<Inscription> inscriptions = new ArrayList<>();
        List<User> users = new ArrayList<>();

        XSSFWorkbook workbook = new XSSFWorkbook(multipart.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 1; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                Etudiant etudiant = new Etudiant();
                User user = new User();
                Inscription inscription = new Inscription();

                XSSFRow row = worksheet.getRow(index);

                // Long id = (long) row.getCell(0).getNumericCellValue();
                // etudiant.setId(id);

                etudiant.setNumIdentifiant(row.getCell(0).getStringCellValue());
                user.setFirstName(row.getCell(1).getStringCellValue());
                user.setLastName(row.getCell(2).getStringCellValue());
                etudiant.setDateNaissance(row.getCell(3).getDateCellValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                etudiant.setLieuNaissance(row.getCell(4).getStringCellValue().toUpperCase());
                user.setEmail(row.getCell(5).getStringCellValue());
                etudiant.setEmail(user.getEmail());
                Niveau niveau = niveauRepository.findByCodeNiveau(row.getCell(6).getStringCellValue());
                etudiant.setUser(user);
                inscription.setNiveau(niveau);
                //etudiant.addEtudiant(inscription);
                inscription.setEtudiant(etudiant);
                EtudiantList.add(etudiant);
                inscriptions.add(inscription);
                users.add(user);
                etudiantRepository.save(etudiant);
                inscriptionRepository.save(inscription);
                userRepository.save(user);
            }
        }
        workbook.close();
        return EtudiantList;
    }

    public void saveEtudiant(List<Etudiant> etudiants){
        for(Etudiant etudiant : etudiants){
            etudiantRepository.save(etudiant);
        }
    }

    // public
}
