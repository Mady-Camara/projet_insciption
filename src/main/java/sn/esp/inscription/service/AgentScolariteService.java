package sn.esp.inscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.esp.inscription.domain.*;
import sn.esp.inscription.domain.enumeration.EnumSituation;
import sn.esp.inscription.repository.AgentScolariteRepository;
import sn.esp.inscription.repository.InscriptionRepository;

import java.nio.file.FileAlreadyExistsException;

@Service
public class AgentScolariteService {
    @Autowired
    AgentScolariteRepository agentScolariteRepository;

    @Autowired
    InscriptionRepository inscriptionRepository;

    public AgentScolarite saveAgentScolarite(AgentScolarite agentScolarite) throws FileAlreadyExistsException {
        AgentScolarite find = agentScolariteRepository.findByMatricule(agentScolarite.getMatricule());
        if(find != null){
            throw new FileAlreadyExistsException("La matricule: "+agentScolarite.getMatricule()+" existe deja");
        }
        return agentScolariteRepository.save(agentScolarite);
    }

    public void updateInscription(Inscription inscription, boolean estBoursier, boolean enReglebibio, boolean estApte, boolean estAssurer, EnumSituation situation, Niveau niveau, AnneeUniversitaire anneeUniversitaire, Etudiant etudiant){
        inscription.setEstApte(estApte);
        inscription.setEstBoursier(estBoursier);
        inscription.setEstAssure(estAssurer);
        inscription.setEnRegleBiblio(enReglebibio);
        inscription.setSituationMatrimoniale(situation);
        inscription.setNiveau(niveau);
        inscription.setAnneeUniversitaire(anneeUniversitaire);
        inscription.setEtudiant(etudiant);
        inscriptionRepository.save(inscription);
    }
}
