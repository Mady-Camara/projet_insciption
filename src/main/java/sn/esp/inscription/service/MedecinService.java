package sn.esp.inscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sn.esp.inscription.domain.Authority;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.domain.Medecin;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.repository.AuthorityRepository;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.repository.MedecinRepository;
import sn.esp.inscription.repository.UserRepository;
import sn.esp.inscription.security.AuthoritiesConstants;
import sn.esp.inscription.web.rest.errors.LoginAlreadyUsedException;
import sn.esp.inscription.web.rest.errors.MatriculeNotFound;

import java.util.HashSet;
import java.util.Set;

@Service
public class MedecinService {
    @Autowired
    MedecinRepository medecinRepository;

    @Autowired
    InscriptionRepository inscriptionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public Medecin createMedecin(Medecin medecin) {
        if(medecinRepository.findByMatricule(medecin.getMatricule()) != null){
            throw new MatriculeNotFound();
        }
        medecin.setMatricule(medecin.getMatricule());

        User user = new User();
        if(userRepository.findOneByLogin(medecin.getEmail()).isPresent()){
            throw new LoginAlreadyUsedException();
        }
        user.setLogin(medecin.getEmail());
        user.setEmail(medecin.getEmail());
        //Encryptage mot de passe
        String EncodingPassword = passwordEncoder.encode(medecin.getPassword());
        user.setPassword(EncodingPassword);
        //user.setLangKey();
        user.setActivated(true);
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.MEDECIN).ifPresent(authorities::add);
        user.setAuthorities(authorities);
        medecin.setUser(userRepository.save(user));
        //userRepository.save(user);
        return medecinRepository.save(medecin);
    }

    public void updateInscription(Inscription inscription, boolean estApte){
        inscription.setEstApte(estApte);
        inscriptionRepository.save(inscription);
    }
}
