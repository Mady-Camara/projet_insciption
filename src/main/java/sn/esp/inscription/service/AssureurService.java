package sn.esp.inscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sn.esp.inscription.domain.Assureur;
import sn.esp.inscription.domain.Authority;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.repository.AssureurRepository;
import sn.esp.inscription.repository.AuthorityRepository;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.repository.UserRepository;
import sn.esp.inscription.security.AuthoritiesConstants;
import sn.esp.inscription.web.rest.errors.LoginAlreadyUsedException;
import sn.esp.inscription.web.rest.errors.MatriculeNotFound;

import java.util.HashSet;
import java.util.Set;

@Service
public class AssureurService {
    @Autowired
    AssureurRepository assureurRepository;

    @Autowired
    InscriptionRepository inscriptionRepository;

    @Autowired
    UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    public AssureurService(PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository){
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }

    public Assureur createAssureur(Assureur assureur) {
        if(assureurRepository.findByMatricule(assureur.getMatricule()) != null){
            throw new MatriculeNotFound();
        }
        assureur.setMatricule(assureur.getMatricule());

        User user = new User();
        if(userRepository.findOneByLogin(assureur.getEmail()).isPresent()){
            throw new LoginAlreadyUsedException();
        }
        user.setLogin(assureur.getEmail());
        user.setEmail(assureur.getEmail());
        //Encryptage mot de passe
        String EncodingPassword = passwordEncoder.encode(assureur.getPassword());
        user.setPassword(EncodingPassword);
        //user.setLangKey();
        user.setActivated(true);
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.ASSUREUR).ifPresent(authorities::add);
        user.setAuthorities(authorities);
        assureur.setUser(userRepository.save(user));
        //userRepository.save(user);
        return assureurRepository.save(assureur);
    }

    public void updateInscription(Inscription inscription, boolean estAssure){
        inscription.setEstAssure(estAssure);
        inscriptionRepository.save(inscription);
    }
}
