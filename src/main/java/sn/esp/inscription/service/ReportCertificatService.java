package sn.esp.inscription.service;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import sn.esp.inscription.domain.Etudiant;
import sn.esp.inscription.repository.EtudiantRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportCertificatService {

    @Autowired
    private EtudiantRepository etudiantRepository;

    public String exportReport(String reportFormat) throws FileNotFoundException, JRException {
        String path = "src/main/java/sn/esp/inscription/jasper_files";
        List<Etudiant> etudiants = etudiantRepository.findAll();
        File file = ResourceUtils.getFile("src/main/java/sn/esp/inscription/jasper_files/certificat.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(etudiants);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Create by ", "ESP");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/certificat.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/certificat.pdf");
        }

        return "Rapport genere avec succes : " + path;
    }
}
