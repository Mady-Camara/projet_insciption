package sn.esp.inscription.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Formation.
 */
@Entity
@Table(name = "formation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Formation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2)
    @Column(name = "code_formation", nullable = false, unique = true)
    private String codeFormation;

    @NotNull
    @Column(name = "libelle_long", nullable = false)
    private String libelleLong;

    @ManyToOne
    @JsonIgnoreProperties(value = "departements", allowSetters = true)
    private Departement departement;

    @OneToMany(mappedBy = "formation")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Niveau> formations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeFormation() {
        return codeFormation;
    }

    public Formation codeFormation(String codeFormation) {
        this.codeFormation = codeFormation;
        return this;
    }

    public void setCodeFormation(String codeFormation) {
        this.codeFormation = codeFormation;
    }

    public String getLibelleLong() {
        return libelleLong;
    }

    public Formation libelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
        return this;
    }

    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    public Departement getDepartement() {
        return departement;
    }

    public Formation departement(Departement departement) {
        this.departement = departement;
        return this;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public Set<Niveau> getFormations() {
        return formations;
    }

    public Formation formations(Set<Niveau> niveaus) {
        this.formations = niveaus;
        return this;
    }

    public Formation addFormation(Niveau niveau) {
        this.formations.add(niveau);
        niveau.setFormation(this);
        return this;
    }

    public Formation removeFormation(Niveau niveau) {
        this.formations.remove(niveau);
        niveau.setFormation(null);
        return this;
    }

    public void setFormations(Set<Niveau> niveaus) {
        this.formations = niveaus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Formation)) {
            return false;
        }
        return id != null && id.equals(((Formation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Formation{" +
            "id=" + getId() +
            ", codeFormation='" + getCodeFormation() + "'" +
            ", libelleLong='" + getLibelleLong() + "'" +
            "}";
    }
}
