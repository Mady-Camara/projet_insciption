package sn.esp.inscription.domain.enumeration;

/**
 * The EnumTypeDept enumeration.
 */
public enum EnumTypeDept {
    Departement, Service
}
