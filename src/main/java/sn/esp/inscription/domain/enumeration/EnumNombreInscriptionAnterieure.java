package sn.esp.inscription.domain.enumeration;

/**
 * The EnumNombreInscriptionAnterieure enumeration.
 */
public enum EnumNombreInscriptionAnterieure {
    ANNEE1, ANNEE2
}
