package sn.esp.inscription.domain.enumeration;

/**
 * The EnumNatureBourse enumeration.
 */
public enum EnumNatureBourse {
    NATIONALE, ETRANGERE, DEETABLISSEMENT
}
