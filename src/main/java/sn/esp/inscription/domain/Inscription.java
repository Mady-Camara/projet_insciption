package sn.esp.inscription.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import sn.esp.inscription.domain.enumeration.EnumHoraireTd;
import sn.esp.inscription.domain.enumeration.EnumNombreInscriptionAnterieure;
import sn.esp.inscription.domain.enumeration.EnumSituation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A Inscription.
 */
@Entity
@Table(name = "inscription")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Inscription implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "est_apte")
    private Boolean estApte;

    @Column(name = "est_boursier")
    private Boolean estBoursier;

    @Column(name = "est_assure")
    private Boolean estAssure;

    @Column(name = "en_regle_biblio")
    private Boolean enRegleBiblio;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "situation_matrimoniale", nullable = false)
    private EnumSituation situationMatrimoniale;

    @Column(name = "annee_etude")
    private String anneeEtude;

    @Column(name = "cycle")
    private Integer cycle;

    @Column(name = "departement")
    private String departement;

    @Column(name = "option_choisie")
    private String optionChoisie;

    @Enumerated(EnumType.STRING)
    @Column(name = "nombre_inscription_anterieur")
    private EnumNombreInscriptionAnterieure nombreInscriptionAnterieur;

    @Column(name = "redoublez_vous")
    private Boolean redoublezVous;

    @Enumerated(EnumType.STRING)
    @Column(name = "horaire_des_td")
    private EnumHoraireTd horaireDesTd;

    @ManyToOne
    @JsonIgnoreProperties(value = "niveaus", allowSetters = true)
    private Niveau niveau;

    @ManyToOne
    @JsonIgnoreProperties(value = "anneeUniversitaires", allowSetters = true)
    private AnneeUniversitaire anneeUniversitaire;

    @ManyToOne
    @JsonIgnoreProperties(value = "etudiants", allowSetters = true)
    private Etudiant etudiant;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEstApte() {
        return estApte;
    }

    public Inscription estApte(Boolean estApte) {
        this.estApte = estApte;
        return this;
    }

    public void setEstApte(Boolean estApte) {
        this.estApte = estApte;
    }

    public Boolean isEstBoursier() {
        return estBoursier;
    }

    public Inscription estBoursier(Boolean estBoursier) {
        this.estBoursier = estBoursier;
        return this;
    }

    public void setEstBoursier(Boolean estBoursier) {
        this.estBoursier = estBoursier;
    }

    public Boolean isEstAssure() {
        return estAssure;
    }

    public Inscription estAssure(Boolean estAssure) {
        this.estAssure = estAssure;
        return this;
    }

    public void setEstAssure(Boolean estAssure) {
        this.estAssure = estAssure;
    }

    public Boolean isEnRegleBiblio() {
        return enRegleBiblio;
    }

    public Inscription enRegleBiblio(Boolean enRegleBiblio) {
        this.enRegleBiblio = enRegleBiblio;
        return this;
    }

    public void setEnRegleBiblio(Boolean enRegleBiblio) {
        this.enRegleBiblio = enRegleBiblio;
    }

    public EnumSituation getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public Inscription situationMatrimoniale(EnumSituation situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
        return this;
    }

    public void setSituationMatrimoniale(EnumSituation situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public String getAnneeEtude() {
        return anneeEtude;
    }

    public Inscription anneeEtude(String anneeEtude) {
        this.anneeEtude = anneeEtude;
        return this;
    }

    public void setAnneeEtude(String anneeEtude) {
        this.anneeEtude = anneeEtude;
    }

    public Integer getCycle() {
        return cycle;
    }

    public Inscription cycle(Integer cycle) {
        this.cycle = cycle;
        return this;
    }

    public void setCycle(Integer cycle) {
        this.cycle = cycle;
    }

    public String getDepartement() {
        return departement;
    }

    public Inscription departement(String departement) {
        this.departement = departement;
        return this;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getOptionChoisie() {
        return optionChoisie;
    }

    public Inscription optionChoisie(String optionChoisie) {
        this.optionChoisie = optionChoisie;
        return this;
    }

    public void setOptionChoisie(String optionChoisie) {
        this.optionChoisie = optionChoisie;
    }

    public EnumNombreInscriptionAnterieure getNombreInscriptionAnterieur() {
        return nombreInscriptionAnterieur;
    }

    public Inscription nombreInscriptionAnterieur(EnumNombreInscriptionAnterieure nombreInscriptionAnterieur) {
        this.nombreInscriptionAnterieur = nombreInscriptionAnterieur;
        return this;
    }

    public void setNombreInscriptionAnterieur(EnumNombreInscriptionAnterieure nombreInscriptionAnterieur) {
        this.nombreInscriptionAnterieur = nombreInscriptionAnterieur;
    }

    public Boolean isRedoublezVous() {
        return redoublezVous;
    }

    public Inscription redoublezVous(Boolean redoublezVous) {
        this.redoublezVous = redoublezVous;
        return this;
    }

    public void setRedoublezVous(Boolean redoublezVous) {
        this.redoublezVous = redoublezVous;
    }

    public EnumHoraireTd getHoraireDesTd() {
        return horaireDesTd;
    }

    public Inscription horaireDesTd(EnumHoraireTd horaireDesTd) {
        this.horaireDesTd = horaireDesTd;
        return this;
    }

    public void setHoraireDesTd(EnumHoraireTd horaireDesTd) {
        this.horaireDesTd = horaireDesTd;
    }

    public Niveau getNiveau() {
        return niveau;
    }

    public Inscription niveau(Niveau niveau) {
        this.niveau = niveau;
        return this;
    }

    public void setNiveau(Niveau niveau) {
        this.niveau = niveau;
    }

    public AnneeUniversitaire getAnneeUniversitaire() {
        return anneeUniversitaire;
    }

    public Inscription anneeUniversitaire(AnneeUniversitaire anneeUniversitaire) {
        this.anneeUniversitaire = anneeUniversitaire;
        return this;
    }

    public void setAnneeUniversitaire(AnneeUniversitaire anneeUniversitaire) {
        this.anneeUniversitaire = anneeUniversitaire;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public Inscription etudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
        return this;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Inscription)) {
            return false;
        }
        return id != null && id.equals(((Inscription) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Inscription{" +
            "id=" + getId() +
            ", estApte='" + isEstApte() + "'" +
            ", estBoursier='" + isEstBoursier() + "'" +
            ", estAssure='" + isEstAssure() + "'" +
            ", enRegleBiblio='" + isEnRegleBiblio() + "'" +
            ", situationMatrimoniale='" + getSituationMatrimoniale() + "'" +
            ", anneeEtude='" + getAnneeEtude() + "'" +
            ", cycle=" + getCycle() +
            ", departement='" + getDepartement() + "'" +
            ", optionChoisie='" + getOptionChoisie() + "'" +
            ", nombreInscriptionAnterieur='" + getNombreInscriptionAnterieur() + "'" +
            ", redoublezVous='" + isRedoublezVous() + "'" +
            ", horaireDesTd='" + getHoraireDesTd() + "'" +
            "}";
    }
}
