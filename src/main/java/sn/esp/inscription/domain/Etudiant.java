package sn.esp.inscription.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import sn.esp.inscription.domain.enumeration.EnumBourse;
import sn.esp.inscription.domain.enumeration.EnumNatureBourse;
import sn.esp.inscription.domain.enumeration.EnumSexe;
import sn.esp.inscription.domain.enumeration.EnumStatusEtudiant;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Etudiant.
 */
@Entity
@Table(name = "etudiant")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Etudiant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 6, max = 9)
    @Column(name = "num_identifiant", length = 9, nullable = false, unique = true)
    private String numIdentifiant;

    @NotNull
    @Column(name = "date_naissance", nullable = false)
    private LocalDate dateNaissance;

    @NotNull
    @Column(name = "lieu_naissance", nullable = false)
    private String lieuNaissance;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sexe", nullable = false)
    private EnumSexe sexe;

    @NotNull
    @Column(name = "ine", nullable = false)
    private String ine;

    @NotNull
    @Size(min = 7)
    @Column(name = "telephone", nullable = false, unique = true)
    private String telephone;

    @NotNull
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "nom_du_mari")
    private String nomDuMari;

    @Column(name = "region_de_naissance")
    private String regionDeNaissance;

    @Column(name = "prenom_2")
    private String prenom2;

    @Column(name = "prenom_3")
    private String prenom3;

    @Column(name = "pays_de_naissance")
    private String paysDeNaissance;

    @Column(name = "nationalite")
    private String nationalite;

    @Column(name = "addresse_a_dakar")
    private String addresseADakar;

    @Column(name = "boite_postal")
    private String boitePostal;

    @Column(name = "portable")
    private String portable;

    @Column(name = "activite_salariee")
    private Boolean activiteSalariee;

    @Column(name = "categorie_socioprofessionnelle")
    private String categorieSocioprofessionnelle;

    @Column(name = "situation_familiale")
    private String situationFamiliale;

    @Column(name = "nombre_enfant")
    private Integer nombreEnfant;

    @Enumerated(EnumType.STRING)
    @Column(name = "bourse")
    private EnumBourse bourse;

    @Enumerated(EnumType.STRING)
    @Column(name = "nature_bourse")
    private EnumNatureBourse natureBourse;

    @Column(name = "montant_bourse")
    private Integer montantBourse;

    @Column(name = "organisme_boursier")
    private String organismeBoursier;

    @Column(name = "serie_diplome")
    private String serieDiplome;

    @Column(name = "annee_diplome")
    private String anneeDiplome;

    @Column(name = "mention_diplome")
    private String mentionDiplome;

    @Column(name = "lieu_diplome")
    private String lieuDiplome;

    @Column(name = "serie_duel_dues_dut_bts")
    private String serieDuelDuesDutBts;

    @Column(name = "annee_duel_dues_dut_bts")
    private String anneeDuelDuesDutBts;

    @Column(name = "mention_duel_dues_dut_bts")
    private String mentionDuelDuesDutBts;

    @Column(name = "lieu_duel_dues_dut_bts")
    private String lieuDuelDuesDutBts;

    @Column(name = "serie_licence_complete")
    private String serieLicenceComplete;

    @Column(name = "annee_licence_complete")
    private String anneeLicenceComplete;

    @Column(name = "mention_licence_complete")
    private String mentionLicenceComplete;

    @Column(name = "lieu_licence_complete")
    private String lieuLicenceComplete;

    @Column(name = "serie_master")
    private String serieMaster;

    @Column(name = "annee_master")
    private String anneeMaster;

    @Column(name = "mention_master")
    private String mentionMaster;

    @Column(name = "lieu")
    private String lieu;

    @Column(name = "serie_doctorat")
    private String serieDoctorat;

    @Column(name = "annee_doctorat")
    private String anneeDoctorat;

    @Column(name = "mention_doctorat")
    private String mentionDoctorat;

    @Column(name = "lieu_doctorat")
    private String lieuDoctorat;

    @Column(name = "nom_personne_a_contacter")
    private String nomPersonneAContacter;

    @Column(name = "nom_du_mari_personne_a_contacter")
    private String nomDuMariPersonneAContacter;

    @Column(name = "lien_de_parente_personne_a_contacter")
    private String lienDeParentePersonneAContacter;

    @Column(name = "adresse_personne_a_contacter")
    private String adressePersonneAContacter;

    @Column(name = "rue_quartier_personne_a_contacter")
    private String rueQuartierPersonneAContacter;

    @Column(name = "ville_personne_a_contacter")
    private String villePersonneAContacter;

    @Column(name = "telephone_personne_a_contacter")
    private String telephonePersonneAContacter;

    @Column(name = "prenom_personne_a_contacter")
    private String prenomPersonneAContacter;

    @Column(name = "prenom_2_personne_a_contacter")
    private String prenom2PersonneAContacter;

    @Column(name = "prenom_3_personne_a_contacter")
    private String prenom3PersonneAContacter;

    @Column(name = "boite_postale_personne_a_contacter")
    private String boitePostalePersonneAContacter;

    @Column(name = "port_personne_a_contacter")
    private String portPersonneAContacter;

    @Column(name = "fax_personne_a_contacter")
    private String faxPersonneAContacter;

    @Column(name = "emial_personne_a_contacter")
    private String emialPersonneAContacter;

    @Column(name = "responsable_est_etudiant")
    private Boolean responsableEstEtudiant;

    @Column(name = "personne_a_contacter")
    private Boolean personneAContacter;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_etudiant")
    private EnumStatusEtudiant statusEtudiant;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "etudiant")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Inscription> etudiants = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumIdentifiant() {
        return numIdentifiant;
    }

    public Etudiant numIdentifiant(String numIdentifiant) {
        this.numIdentifiant = numIdentifiant;
        return this;
    }

    public void setNumIdentifiant(String numIdentifiant) {
        this.numIdentifiant = numIdentifiant;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public Etudiant dateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
        return this;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public Etudiant lieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
        return this;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public EnumSexe getSexe() {
        return sexe;
    }

    public Etudiant sexe(EnumSexe sexe) {
        this.sexe = sexe;
        return this;
    }

    public void setSexe(EnumSexe sexe) {
        this.sexe = sexe;
    }

    public String getIne() {
        return ine;
    }

    public Etudiant ine(String ine) {
        this.ine = ine;
        return this;
    }

    public void setIne(String ine) {
        this.ine = ine;
    }

    public String getTelephone() {
        return telephone;
    }

    public Etudiant telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public Etudiant email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomDuMari() {
        return nomDuMari;
    }

    public Etudiant nomDuMari(String nomDuMari) {
        this.nomDuMari = nomDuMari;
        return this;
    }

    public void setNomDuMari(String nomDuMari) {
        this.nomDuMari = nomDuMari;
    }

    public String getRegionDeNaissance() {
        return regionDeNaissance;
    }

    public Etudiant regionDeNaissance(String regionDeNaissance) {
        this.regionDeNaissance = regionDeNaissance;
        return this;
    }

    public void setRegionDeNaissance(String regionDeNaissance) {
        this.regionDeNaissance = regionDeNaissance;
    }

    public String getPrenom2() {
        return prenom2;
    }

    public Etudiant prenom2(String prenom2) {
        this.prenom2 = prenom2;
        return this;
    }

    public void setPrenom2(String prenom2) {
        this.prenom2 = prenom2;
    }

    public String getPrenom3() {
        return prenom3;
    }

    public Etudiant prenom3(String prenom3) {
        this.prenom3 = prenom3;
        return this;
    }

    public void setPrenom3(String prenom3) {
        this.prenom3 = prenom3;
    }

    public String getPaysDeNaissance() {
        return paysDeNaissance;
    }

    public Etudiant paysDeNaissance(String paysDeNaissance) {
        this.paysDeNaissance = paysDeNaissance;
        return this;
    }

    public void setPaysDeNaissance(String paysDeNaissance) {
        this.paysDeNaissance = paysDeNaissance;
    }

    public String getNationalite() {
        return nationalite;
    }

    public Etudiant nationalite(String nationalite) {
        this.nationalite = nationalite;
        return this;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public String getAddresseADakar() {
        return addresseADakar;
    }

    public Etudiant addresseADakar(String addresseADakar) {
        this.addresseADakar = addresseADakar;
        return this;
    }

    public void setAddresseADakar(String addresseADakar) {
        this.addresseADakar = addresseADakar;
    }

    public String getBoitePostal() {
        return boitePostal;
    }

    public Etudiant boitePostal(String boitePostal) {
        this.boitePostal = boitePostal;
        return this;
    }

    public void setBoitePostal(String boitePostal) {
        this.boitePostal = boitePostal;
    }

    public String getPortable() {
        return portable;
    }

    public Etudiant portable(String portable) {
        this.portable = portable;
        return this;
    }

    public void setPortable(String portable) {
        this.portable = portable;
    }

    public Boolean isActiviteSalariee() {
        return activiteSalariee;
    }

    public Etudiant activiteSalariee(Boolean activiteSalariee) {
        this.activiteSalariee = activiteSalariee;
        return this;
    }

    public void setActiviteSalariee(Boolean activiteSalariee) {
        this.activiteSalariee = activiteSalariee;
    }

    public String getCategorieSocioprofessionnelle() {
        return categorieSocioprofessionnelle;
    }

    public Etudiant categorieSocioprofessionnelle(String categorieSocioprofessionnelle) {
        this.categorieSocioprofessionnelle = categorieSocioprofessionnelle;
        return this;
    }

    public void setCategorieSocioprofessionnelle(String categorieSocioprofessionnelle) {
        this.categorieSocioprofessionnelle = categorieSocioprofessionnelle;
    }

    public String getSituationFamiliale() {
        return situationFamiliale;
    }

    public Etudiant situationFamiliale(String situationFamiliale) {
        this.situationFamiliale = situationFamiliale;
        return this;
    }

    public void setSituationFamiliale(String situationFamiliale) {
        this.situationFamiliale = situationFamiliale;
    }

    public Integer getNombreEnfant() {
        return nombreEnfant;
    }

    public Etudiant nombreEnfant(Integer nombreEnfant) {
        this.nombreEnfant = nombreEnfant;
        return this;
    }

    public void setNombreEnfant(Integer nombreEnfant) {
        this.nombreEnfant = nombreEnfant;
    }

    public EnumBourse getBourse() {
        return bourse;
    }

    public Etudiant bourse(EnumBourse bourse) {
        this.bourse = bourse;
        return this;
    }

    public void setBourse(EnumBourse bourse) {
        this.bourse = bourse;
    }

    public EnumNatureBourse getNatureBourse() {
        return natureBourse;
    }

    public Etudiant natureBourse(EnumNatureBourse natureBourse) {
        this.natureBourse = natureBourse;
        return this;
    }

    public void setNatureBourse(EnumNatureBourse natureBourse) {
        this.natureBourse = natureBourse;
    }

    public Integer getMontantBourse() {
        return montantBourse;
    }

    public Etudiant montantBourse(Integer montantBourse) {
        this.montantBourse = montantBourse;
        return this;
    }

    public void setMontantBourse(Integer montantBourse) {
        this.montantBourse = montantBourse;
    }

    public String getOrganismeBoursier() {
        return organismeBoursier;
    }

    public Etudiant organismeBoursier(String organismeBoursier) {
        this.organismeBoursier = organismeBoursier;
        return this;
    }

    public void setOrganismeBoursier(String organismeBoursier) {
        this.organismeBoursier = organismeBoursier;
    }

    public String getSerieDiplome() {
        return serieDiplome;
    }

    public Etudiant serieDiplome(String serieDiplome) {
        this.serieDiplome = serieDiplome;
        return this;
    }

    public void setSerieDiplome(String serieDiplome) {
        this.serieDiplome = serieDiplome;
    }

    public String getAnneeDiplome() {
        return anneeDiplome;
    }

    public Etudiant anneeDiplome(String anneeDiplome) {
        this.anneeDiplome = anneeDiplome;
        return this;
    }

    public void setAnneeDiplome(String anneeDiplome) {
        this.anneeDiplome = anneeDiplome;
    }

    public String getMentionDiplome() {
        return mentionDiplome;
    }

    public Etudiant mentionDiplome(String mentionDiplome) {
        this.mentionDiplome = mentionDiplome;
        return this;
    }

    public void setMentionDiplome(String mentionDiplome) {
        this.mentionDiplome = mentionDiplome;
    }

    public String getLieuDiplome() {
        return lieuDiplome;
    }

    public Etudiant lieuDiplome(String lieuDiplome) {
        this.lieuDiplome = lieuDiplome;
        return this;
    }

    public void setLieuDiplome(String lieuDiplome) {
        this.lieuDiplome = lieuDiplome;
    }

    public String getSerieDuelDuesDutBts() {
        return serieDuelDuesDutBts;
    }

    public Etudiant serieDuelDuesDutBts(String serieDuelDuesDutBts) {
        this.serieDuelDuesDutBts = serieDuelDuesDutBts;
        return this;
    }

    public void setSerieDuelDuesDutBts(String serieDuelDuesDutBts) {
        this.serieDuelDuesDutBts = serieDuelDuesDutBts;
    }

    public String getAnneeDuelDuesDutBts() {
        return anneeDuelDuesDutBts;
    }

    public Etudiant anneeDuelDuesDutBts(String anneeDuelDuesDutBts) {
        this.anneeDuelDuesDutBts = anneeDuelDuesDutBts;
        return this;
    }

    public void setAnneeDuelDuesDutBts(String anneeDuelDuesDutBts) {
        this.anneeDuelDuesDutBts = anneeDuelDuesDutBts;
    }

    public String getMentionDuelDuesDutBts() {
        return mentionDuelDuesDutBts;
    }

    public Etudiant mentionDuelDuesDutBts(String mentionDuelDuesDutBts) {
        this.mentionDuelDuesDutBts = mentionDuelDuesDutBts;
        return this;
    }

    public void setMentionDuelDuesDutBts(String mentionDuelDuesDutBts) {
        this.mentionDuelDuesDutBts = mentionDuelDuesDutBts;
    }

    public String getLieuDuelDuesDutBts() {
        return lieuDuelDuesDutBts;
    }

    public Etudiant lieuDuelDuesDutBts(String lieuDuelDuesDutBts) {
        this.lieuDuelDuesDutBts = lieuDuelDuesDutBts;
        return this;
    }

    public void setLieuDuelDuesDutBts(String lieuDuelDuesDutBts) {
        this.lieuDuelDuesDutBts = lieuDuelDuesDutBts;
    }

    public String getSerieLicenceComplete() {
        return serieLicenceComplete;
    }

    public Etudiant serieLicenceComplete(String serieLicenceComplete) {
        this.serieLicenceComplete = serieLicenceComplete;
        return this;
    }

    public void setSerieLicenceComplete(String serieLicenceComplete) {
        this.serieLicenceComplete = serieLicenceComplete;
    }

    public String getAnneeLicenceComplete() {
        return anneeLicenceComplete;
    }

    public Etudiant anneeLicenceComplete(String anneeLicenceComplete) {
        this.anneeLicenceComplete = anneeLicenceComplete;
        return this;
    }

    public void setAnneeLicenceComplete(String anneeLicenceComplete) {
        this.anneeLicenceComplete = anneeLicenceComplete;
    }

    public String getMentionLicenceComplete() {
        return mentionLicenceComplete;
    }

    public Etudiant mentionLicenceComplete(String mentionLicenceComplete) {
        this.mentionLicenceComplete = mentionLicenceComplete;
        return this;
    }

    public void setMentionLicenceComplete(String mentionLicenceComplete) {
        this.mentionLicenceComplete = mentionLicenceComplete;
    }

    public String getLieuLicenceComplete() {
        return lieuLicenceComplete;
    }

    public Etudiant lieuLicenceComplete(String lieuLicenceComplete) {
        this.lieuLicenceComplete = lieuLicenceComplete;
        return this;
    }

    public void setLieuLicenceComplete(String lieuLicenceComplete) {
        this.lieuLicenceComplete = lieuLicenceComplete;
    }

    public String getSerieMaster() {
        return serieMaster;
    }

    public Etudiant serieMaster(String serieMaster) {
        this.serieMaster = serieMaster;
        return this;
    }

    public void setSerieMaster(String serieMaster) {
        this.serieMaster = serieMaster;
    }

    public String getAnneeMaster() {
        return anneeMaster;
    }

    public Etudiant anneeMaster(String anneeMaster) {
        this.anneeMaster = anneeMaster;
        return this;
    }

    public void setAnneeMaster(String anneeMaster) {
        this.anneeMaster = anneeMaster;
    }

    public String getMentionMaster() {
        return mentionMaster;
    }

    public Etudiant mentionMaster(String mentionMaster) {
        this.mentionMaster = mentionMaster;
        return this;
    }

    public void setMentionMaster(String mentionMaster) {
        this.mentionMaster = mentionMaster;
    }

    public String getLieu() {
        return lieu;
    }

    public Etudiant lieu(String lieu) {
        this.lieu = lieu;
        return this;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getSerieDoctorat() {
        return serieDoctorat;
    }

    public Etudiant serieDoctorat(String serieDoctorat) {
        this.serieDoctorat = serieDoctorat;
        return this;
    }

    public void setSerieDoctorat(String serieDoctorat) {
        this.serieDoctorat = serieDoctorat;
    }

    public String getAnneeDoctorat() {
        return anneeDoctorat;
    }

    public Etudiant anneeDoctorat(String anneeDoctorat) {
        this.anneeDoctorat = anneeDoctorat;
        return this;
    }

    public void setAnneeDoctorat(String anneeDoctorat) {
        this.anneeDoctorat = anneeDoctorat;
    }

    public String getMentionDoctorat() {
        return mentionDoctorat;
    }

    public Etudiant mentionDoctorat(String mentionDoctorat) {
        this.mentionDoctorat = mentionDoctorat;
        return this;
    }

    public void setMentionDoctorat(String mentionDoctorat) {
        this.mentionDoctorat = mentionDoctorat;
    }

    public String getLieuDoctorat() {
        return lieuDoctorat;
    }

    public Etudiant lieuDoctorat(String lieuDoctorat) {
        this.lieuDoctorat = lieuDoctorat;
        return this;
    }

    public void setLieuDoctorat(String lieuDoctorat) {
        this.lieuDoctorat = lieuDoctorat;
    }

    public String getNomPersonneAContacter() {
        return nomPersonneAContacter;
    }

    public Etudiant nomPersonneAContacter(String nomPersonneAContacter) {
        this.nomPersonneAContacter = nomPersonneAContacter;
        return this;
    }

    public void setNomPersonneAContacter(String nomPersonneAContacter) {
        this.nomPersonneAContacter = nomPersonneAContacter;
    }

    public String getNomDuMariPersonneAContacter() {
        return nomDuMariPersonneAContacter;
    }

    public Etudiant nomDuMariPersonneAContacter(String nomDuMariPersonneAContacter) {
        this.nomDuMariPersonneAContacter = nomDuMariPersonneAContacter;
        return this;
    }

    public void setNomDuMariPersonneAContacter(String nomDuMariPersonneAContacter) {
        this.nomDuMariPersonneAContacter = nomDuMariPersonneAContacter;
    }

    public String getLienDeParentePersonneAContacter() {
        return lienDeParentePersonneAContacter;
    }

    public Etudiant lienDeParentePersonneAContacter(String lienDeParentePersonneAContacter) {
        this.lienDeParentePersonneAContacter = lienDeParentePersonneAContacter;
        return this;
    }

    public void setLienDeParentePersonneAContacter(String lienDeParentePersonneAContacter) {
        this.lienDeParentePersonneAContacter = lienDeParentePersonneAContacter;
    }

    public String getAdressePersonneAContacter() {
        return adressePersonneAContacter;
    }

    public Etudiant adressePersonneAContacter(String adressePersonneAContacter) {
        this.adressePersonneAContacter = adressePersonneAContacter;
        return this;
    }

    public void setAdressePersonneAContacter(String adressePersonneAContacter) {
        this.adressePersonneAContacter = adressePersonneAContacter;
    }

    public String getRueQuartierPersonneAContacter() {
        return rueQuartierPersonneAContacter;
    }

    public Etudiant rueQuartierPersonneAContacter(String rueQuartierPersonneAContacter) {
        this.rueQuartierPersonneAContacter = rueQuartierPersonneAContacter;
        return this;
    }

    public void setRueQuartierPersonneAContacter(String rueQuartierPersonneAContacter) {
        this.rueQuartierPersonneAContacter = rueQuartierPersonneAContacter;
    }

    public String getVillePersonneAContacter() {
        return villePersonneAContacter;
    }

    public Etudiant villePersonneAContacter(String villePersonneAContacter) {
        this.villePersonneAContacter = villePersonneAContacter;
        return this;
    }

    public void setVillePersonneAContacter(String villePersonneAContacter) {
        this.villePersonneAContacter = villePersonneAContacter;
    }

    public String getTelephonePersonneAContacter() {
        return telephonePersonneAContacter;
    }

    public Etudiant telephonePersonneAContacter(String telephonePersonneAContacter) {
        this.telephonePersonneAContacter = telephonePersonneAContacter;
        return this;
    }

    public void setTelephonePersonneAContacter(String telephonePersonneAContacter) {
        this.telephonePersonneAContacter = telephonePersonneAContacter;
    }

    public String getPrenomPersonneAContacter() {
        return prenomPersonneAContacter;
    }

    public Etudiant prenomPersonneAContacter(String prenomPersonneAContacter) {
        this.prenomPersonneAContacter = prenomPersonneAContacter;
        return this;
    }

    public void setPrenomPersonneAContacter(String prenomPersonneAContacter) {
        this.prenomPersonneAContacter = prenomPersonneAContacter;
    }

    public String getPrenom2PersonneAContacter() {
        return prenom2PersonneAContacter;
    }

    public Etudiant prenom2PersonneAContacter(String prenom2PersonneAContacter) {
        this.prenom2PersonneAContacter = prenom2PersonneAContacter;
        return this;
    }

    public void setPrenom2PersonneAContacter(String prenom2PersonneAContacter) {
        this.prenom2PersonneAContacter = prenom2PersonneAContacter;
    }

    public String getPrenom3PersonneAContacter() {
        return prenom3PersonneAContacter;
    }

    public Etudiant prenom3PersonneAContacter(String prenom3PersonneAContacter) {
        this.prenom3PersonneAContacter = prenom3PersonneAContacter;
        return this;
    }

    public void setPrenom3PersonneAContacter(String prenom3PersonneAContacter) {
        this.prenom3PersonneAContacter = prenom3PersonneAContacter;
    }

    public String getBoitePostalePersonneAContacter() {
        return boitePostalePersonneAContacter;
    }

    public Etudiant boitePostalePersonneAContacter(String boitePostalePersonneAContacter) {
        this.boitePostalePersonneAContacter = boitePostalePersonneAContacter;
        return this;
    }

    public void setBoitePostalePersonneAContacter(String boitePostalePersonneAContacter) {
        this.boitePostalePersonneAContacter = boitePostalePersonneAContacter;
    }

    public String getPortPersonneAContacter() {
        return portPersonneAContacter;
    }

    public Etudiant portPersonneAContacter(String portPersonneAContacter) {
        this.portPersonneAContacter = portPersonneAContacter;
        return this;
    }

    public void setPortPersonneAContacter(String portPersonneAContacter) {
        this.portPersonneAContacter = portPersonneAContacter;
    }

    public String getFaxPersonneAContacter() {
        return faxPersonneAContacter;
    }

    public Etudiant faxPersonneAContacter(String faxPersonneAContacter) {
        this.faxPersonneAContacter = faxPersonneAContacter;
        return this;
    }

    public void setFaxPersonneAContacter(String faxPersonneAContacter) {
        this.faxPersonneAContacter = faxPersonneAContacter;
    }

    public String getEmialPersonneAContacter() {
        return emialPersonneAContacter;
    }

    public Etudiant emialPersonneAContacter(String emialPersonneAContacter) {
        this.emialPersonneAContacter = emialPersonneAContacter;
        return this;
    }

    public void setEmialPersonneAContacter(String emialPersonneAContacter) {
        this.emialPersonneAContacter = emialPersonneAContacter;
    }

    public Boolean isResponsableEstEtudiant() {
        return responsableEstEtudiant;
    }

    public Etudiant responsableEstEtudiant(Boolean responsableEstEtudiant) {
        this.responsableEstEtudiant = responsableEstEtudiant;
        return this;
    }

    public void setResponsableEstEtudiant(Boolean responsableEstEtudiant) {
        this.responsableEstEtudiant = responsableEstEtudiant;
    }

    public Boolean isPersonneAContacter() {
        return personneAContacter;
    }

    public Etudiant personneAContacter(Boolean personneAContacter) {
        this.personneAContacter = personneAContacter;
        return this;
    }

    public void setPersonneAContacter(Boolean personneAContacter) {
        this.personneAContacter = personneAContacter;
    }

    public EnumStatusEtudiant getStatusEtudiant() {
        return statusEtudiant;
    }

    public Etudiant statusEtudiant(EnumStatusEtudiant statusEtudiant) {
        this.statusEtudiant = statusEtudiant;
        return this;
    }

    public void setStatusEtudiant(EnumStatusEtudiant statusEtudiant) {
        this.statusEtudiant = statusEtudiant;
    }

    public User getUser() {
        return user;
    }

    public Etudiant user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Inscription> getEtudiants() {
        return etudiants;
    }

    public Etudiant etudiants(Set<Inscription> inscriptions) {
        this.etudiants = inscriptions;
        return this;
    }

    public Etudiant addEtudiant(Inscription inscription) {
        this.etudiants.add(inscription);
        inscription.setEtudiant(this);
        return this;
    }

    public Etudiant removeEtudiant(Inscription inscription) {
        this.etudiants.remove(inscription);
        inscription.setEtudiant(null);
        return this;
    }

    public void setEtudiants(Set<Inscription> inscriptions) {
        this.etudiants = inscriptions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Etudiant)) {
            return false;
        }
        return id != null && id.equals(((Etudiant) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Etudiant{" +
            "id=" + getId() +
            ", numIdentifiant='" + getNumIdentifiant() + "'" +
            ", dateNaissance='" + getDateNaissance() + "'" +
            ", lieuNaissance='" + getLieuNaissance() + "'" +
            ", sexe='" + getSexe() + "'" +
            ", ine='" + getIne() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", email='" + getEmail() + "'" +
            ", nomDuMari='" + getNomDuMari() + "'" +
            ", regionDeNaissance='" + getRegionDeNaissance() + "'" +
            ", prenom2='" + getPrenom2() + "'" +
            ", prenom3='" + getPrenom3() + "'" +
            ", paysDeNaissance='" + getPaysDeNaissance() + "'" +
            ", nationalite='" + getNationalite() + "'" +
            ", addresseADakar='" + getAddresseADakar() + "'" +
            ", boitePostal='" + getBoitePostal() + "'" +
            ", portable='" + getPortable() + "'" +
            ", activiteSalariee='" + isActiviteSalariee() + "'" +
            ", categorieSocioprofessionnelle='" + getCategorieSocioprofessionnelle() + "'" +
            ", situationFamiliale='" + getSituationFamiliale() + "'" +
            ", nombreEnfant=" + getNombreEnfant() +
            ", bourse='" + getBourse() + "'" +
            ", natureBourse='" + getNatureBourse() + "'" +
            ", montantBourse=" + getMontantBourse() +
            ", organismeBoursier='" + getOrganismeBoursier() + "'" +
            ", serieDiplome='" + getSerieDiplome() + "'" +
            ", anneeDiplome='" + getAnneeDiplome() + "'" +
            ", mentionDiplome='" + getMentionDiplome() + "'" +
            ", lieuDiplome='" + getLieuDiplome() + "'" +
            ", serieDuelDuesDutBts='" + getSerieDuelDuesDutBts() + "'" +
            ", anneeDuelDuesDutBts='" + getAnneeDuelDuesDutBts() + "'" +
            ", mentionDuelDuesDutBts='" + getMentionDuelDuesDutBts() + "'" +
            ", lieuDuelDuesDutBts='" + getLieuDuelDuesDutBts() + "'" +
            ", serieLicenceComplete='" + getSerieLicenceComplete() + "'" +
            ", anneeLicenceComplete='" + getAnneeLicenceComplete() + "'" +
            ", mentionLicenceComplete='" + getMentionLicenceComplete() + "'" +
            ", lieuLicenceComplete='" + getLieuLicenceComplete() + "'" +
            ", serieMaster='" + getSerieMaster() + "'" +
            ", anneeMaster='" + getAnneeMaster() + "'" +
            ", mentionMaster='" + getMentionMaster() + "'" +
            ", lieu='" + getLieu() + "'" +
            ", serieDoctorat='" + getSerieDoctorat() + "'" +
            ", anneeDoctorat='" + getAnneeDoctorat() + "'" +
            ", mentionDoctorat='" + getMentionDoctorat() + "'" +
            ", lieuDoctorat='" + getLieuDoctorat() + "'" +
            ", nomPersonneAContacter='" + getNomPersonneAContacter() + "'" +
            ", nomDuMariPersonneAContacter='" + getNomDuMariPersonneAContacter() + "'" +
            ", lienDeParentePersonneAContacter='" + getLienDeParentePersonneAContacter() + "'" +
            ", adressePersonneAContacter='" + getAdressePersonneAContacter() + "'" +
            ", rueQuartierPersonneAContacter='" + getRueQuartierPersonneAContacter() + "'" +
            ", villePersonneAContacter='" + getVillePersonneAContacter() + "'" +
            ", telephonePersonneAContacter='" + getTelephonePersonneAContacter() + "'" +
            ", prenomPersonneAContacter='" + getPrenomPersonneAContacter() + "'" +
            ", prenom2PersonneAContacter='" + getPrenom2PersonneAContacter() + "'" +
            ", prenom3PersonneAContacter='" + getPrenom3PersonneAContacter() + "'" +
            ", boitePostalePersonneAContacter='" + getBoitePostalePersonneAContacter() + "'" +
            ", portPersonneAContacter='" + getPortPersonneAContacter() + "'" +
            ", faxPersonneAContacter='" + getFaxPersonneAContacter() + "'" +
            ", emialPersonneAContacter='" + getEmialPersonneAContacter() + "'" +
            ", responsableEstEtudiant='" + isResponsableEstEtudiant() + "'" +
            ", personneAContacter='" + isPersonneAContacter() + "'" +
            ", statusEtudiant='" + getStatusEtudiant() + "'" +
            "}";
    }
}
