package sn.esp.inscription.web.rest.errors;

public class TelephoneNotFoundException extends BadRequestAlertException {
    public TelephoneNotFoundException() {
        super(ErrorConstants.TELEPHONE_NOT_FOUND_TYPE, "telephone not found", "etudiant", "telephonedontexist");
    }
}
