package sn.esp.inscription.web.rest.errors;

public class NumIndentifiantNotFoundException extends BadRequestAlertException {
    public NumIndentifiantNotFoundException() {
        super(ErrorConstants.IDENTIFIANT_NOT_FOUND_TYPE, "NumIdentifiant not found", "userManagement", "Numdontexist");
    }
}
