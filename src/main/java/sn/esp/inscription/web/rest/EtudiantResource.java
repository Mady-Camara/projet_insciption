package sn.esp.inscription.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.esp.inscription.domain.Etudiant;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.repository.EtudiantRepository;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.repository.UserRepository;
import sn.esp.inscription.service.EtudiantService;
import sn.esp.inscription.service.ReportCertificatService;
import sn.esp.inscription.service.UserService;
import sn.esp.inscription.service.imports.EtudiantImport;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;
import sn.esp.inscription.web.rest.errors.EmailNotFoundException;
import sn.esp.inscription.web.rest.errors.NumIndentifiantNotFoundException;
import sn.esp.inscription.web.rest.errors.TelephoneNotFoundException;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.Etudiant}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class EtudiantResource {

    private final Logger log = LoggerFactory.getLogger(EtudiantResource.class);

    private static final String ENTITY_NAME = "etudiant";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtudiantRepository etudiantRepository;

    private final EtudiantImport etudiantImport;

    private final ReportCertificatService reportCertificatService;

    private final InscriptionRepository inscriptionRepository;

    private final UserService userService;

    @Autowired
    EtudiantService etudiantService;

    @Autowired
    UserRepository userRepository;

    public EtudiantResource(EtudiantRepository etudiantRepository, ReportCertificatService reportCertificatService, InscriptionRepository inscriptionRepository, UserService userService) {
        this.etudiantImport = new EtudiantImport();
        this.etudiantRepository = etudiantRepository;
        this.reportCertificatService = reportCertificatService;
        this.inscriptionRepository = inscriptionRepository;
        this.userService = userService;
    }

    /**
     * {@code POST  /etudiants} : Create a new etudiant.
     *
     * @param etudiant the etudiant to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new etudiant, or with status {@code 400 (Bad Request)} if the etudiant has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/etudiants")
    public ResponseEntity<Etudiant> createEtudiant(@Valid @RequestBody Etudiant etudiant) throws URISyntaxException {
        log.debug("REST request to save Etudiant : {}", etudiant);
        if (etudiant.getId() != null) {
            throw new BadRequestAlertException("A new etudiant cannot already have an ID", ENTITY_NAME, "idexists");
        }
        /*if(etudiant.getUser() == null){
            throw new BadRequestAlertException("A new etudiant cannot already have an USER", ENTITY_NAME, "userexits");
        }*/
        Etudiant result = etudiantRepository.save(etudiant);
        return ResponseEntity.created(new URI("/api/etudiants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /etudiants} : Updates an existing etudiant.
     *
     * @param etudiant the etudiant to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etudiant,
     * or with status {@code 400 (Bad Request)} if the etudiant is not valid,
     * or with status {@code 500 (Internal Server Error)} if the etudiant couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/etudiants")
    public ResponseEntity<Etudiant> updateEtudiant(@Valid @RequestBody Etudiant etudiant) throws URISyntaxException {
        log.debug("REST request to update Etudiant : {}", etudiant);
        if (etudiant.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Etudiant result = etudiantRepository.save(etudiant);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, etudiant.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /etudiants} : get all the etudiants.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of etudiants in body.
     */
    @GetMapping("/etudiants")
    public ResponseEntity<List<Etudiant>> getAllEtudiants(Pageable pageable) {
        log.debug("REST request to get a page of Etudiants");
        Page<Etudiant> page = etudiantRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /etudiants/:id} : get the "id" etudiant.
     *
     * @param id the id of the etudiant to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the etudiant, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/etudiants/{id}")
    public ResponseEntity<Etudiant> getEtudiant(@PathVariable Long id) {
        log.debug("REST request to get Etudiant : {}", id);
        Optional<Etudiant> etudiant = etudiantRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(etudiant);
    }

    /**
     * {@code DELETE  /etudiants/:id} : delete the "id" etudiant.
     *
     * @param id the id of the etudiant to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/etudiants/{id}")
    public ResponseEntity<Void> deleteEtudiant(@PathVariable Long id) {
        log.debug("REST request to delete Etudiant : {}", id);
        etudiantRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/etudiants/imports")
    public List<Etudiant> importExcelFile(@RequestParam("file") MultipartFile reapExcelDataFile) throws IOException {
        return this.etudiantImport.getEtudiant(reapExcelDataFile);
    }

    @GetMapping("/etudiants/allEtudiants")
    public List<Etudiant> getEtudiant(){
        return etudiantRepository.findAll();
    }

    @GetMapping("/etudiants/report/{format}")
    public String generateReport(@PathVariable String format) throws FileNotFoundException, JRException {
        return reportCertificatService.exportReport(format);
    }

    @PutMapping("etudiants/formulaire")
    public void formulaire(@RequestBody Etudiant etudiant, @RequestBody Inscription inscription){
        if(etudiantRepository.findByNumIdentifiant(etudiant.getNumIdentifiant()) == null){
            throw new NumIndentifiantNotFoundException();
        }
        else if(etudiantRepository.findByEmailIgnoreCase(etudiant.getEmail()) == null){
            throw new EmailNotFoundException();
        }
        else if(etudiantRepository.findByTelephone(etudiant.getTelephone()) == null){
            throw new TelephoneNotFoundException();
        }
        else{
            etudiantRepository.save(etudiant);
            inscriptionRepository.save(inscription);
        }
    }

    @GetMapping("/etudiants/etatInscription/{num}")
    public ResponseEntity<Inscription> etatAssurer(@PathVariable String num){
        Inscription inscription = etudiantService.InscritionActive(num);
        return ResponseEntity.ok().body(inscription);
    }

    @GetMapping("/etudiants/currentUserId")
    public ResponseEntity<Etudiant> idUser(){
        String login = userService.getUserWithAuthorities().get().getLogin();
        User user = userRepository.findByLogin(login);
        System.out.println("========================================> "+login);
        Etudiant etudiant = etudiantRepository.findByUser(user);
        System.out.println("======================< USER >==================> "+user.getLogin());
        if(etudiant == null){
            return ResponseEntity.notFound().build();
        }
        System.out.println("========================< ETUDIANT >================> "+etudiant.getId());
        return ResponseEntity.ok().body(etudiant);
    }

}
