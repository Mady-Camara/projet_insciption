package sn.esp.inscription.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sn.esp.inscription.domain.AgentBiblio;
import sn.esp.inscription.domain.AgentScolarite;
import sn.esp.inscription.domain.Assureur;
import sn.esp.inscription.domain.Medecin;
import sn.esp.inscription.repository.AgentBiblioRepository;
import sn.esp.inscription.repository.AgentScolariteRepository;
import sn.esp.inscription.repository.AssureurRepository;
import sn.esp.inscription.repository.MedecinRepository;
import sn.esp.inscription.service.PdfGeneratorService;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.AgentScolarite}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AgentScolariteResource {

    private final Logger log = LoggerFactory.getLogger(AgentScolariteResource.class);

    private static final String ENTITY_NAME = "agentScolarite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentScolariteRepository agentScolariteRepository;

    private final AssureurRepository assureurRepository;

    private final MedecinRepository medecinRepository;

    private final AgentBiblioRepository agentBiblioRepository;

    public static final String FORMATION_SUB = "FORMATION_SUB";
    public static final String NIVEAU_SUB = "NIVEAU_SUB";
    public static final String FORMULAIRE_PAGE2_SUB = "FORMULAIRE_PAGE2_SUB";

    @Autowired
    PdfGeneratorService pdfGenerator;

    public AgentScolariteResource(AgentScolariteRepository agentScolariteRepository, AssureurRepository assureurRepository, MedecinRepository medecinRepository, AgentBiblioRepository agentBiblioRepository) {
        this.agentScolariteRepository = agentScolariteRepository;
        this.assureurRepository = assureurRepository;
        this.medecinRepository = medecinRepository;
        this.agentBiblioRepository = agentBiblioRepository;
    }

    /**
     * {@code POST  /agent-scolarites} : Create a new agentScolarite.
     *
     * @param agentScolarite the agentScolarite to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agentScolarite, or with status {@code 400 (Bad Request)} if the agentScolarite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agent-scolarites")
    public ResponseEntity<AgentScolarite> createAgentScolarite(@Valid @RequestBody AgentScolarite agentScolarite) throws URISyntaxException {
        log.debug("REST request to save AgentScolarite : {}", agentScolarite);
        if (agentScolarite.getId() != null) {
            throw new BadRequestAlertException("A new agentScolarite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentScolarite result = agentScolariteRepository.save(agentScolarite);
        return ResponseEntity.created(new URI("/api/agent-scolarites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /agent-scolarites} : Updates an existing agentScolarite.
     *
     * @param agentScolarite the agentScolarite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agentScolarite,
     * or with status {@code 400 (Bad Request)} if the agentScolarite is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agentScolarite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agent-scolarites")
    public ResponseEntity<AgentScolarite> updateAgentScolarite(@Valid @RequestBody AgentScolarite agentScolarite) throws URISyntaxException {
        log.debug("REST request to update AgentScolarite : {}", agentScolarite);
        if (agentScolarite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentScolarite result = agentScolariteRepository.save(agentScolarite);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, agentScolarite.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /agent-scolarites} : get all the agentScolarites.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agentScolarites in body.
     */
    @GetMapping("/agent-scolarites")
    public List<AgentScolarite> getAllAgentScolarites() {
        log.debug("REST request to get all AgentScolarites");
        return agentScolariteRepository.findAll();
    }

    /**
     * {@code GET  /agent-scolarites/:id} : get the "id" agentScolarite.
     *
     * @param id the id of the agentScolarite to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agentScolarite, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agent-scolarites/{id}")
    public ResponseEntity<AgentScolarite> getAgentScolarite(@PathVariable Long id) {
        log.debug("REST request to get AgentScolarite : {}", id);
        Optional<AgentScolarite> agentScolarite = agentScolariteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentScolarite);
    }

    /**
     * {@code DELETE  /agent-scolarites/:id} : delete the "id" agentScolarite.
     *
     * @param id the id of the agentScolarite to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agent-scolarites/{id}")
    public ResponseEntity<Void> deleteAgentScolarite(@PathVariable Long id) {
        log.debug("REST request to delete AgentScolarite : {}", id);
        agentScolariteRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/getAssureur/{matricule}")
    public ResponseEntity<Assureur> getAssureur(@PathVariable String matricule){
        log.debug("request to get info etudiant");
        Assureur assureur = assureurRepository.findByMatricule(matricule);
        if(assureur == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(assureur);
    }

    @GetMapping("/getMedecin/{matricule}")
    public ResponseEntity<Medecin> getMedecin(@PathVariable String matricule){
        log.debug("request to get info etudiant");
        Medecin medecin = medecinRepository.findByMatricule(matricule);
        if(medecin == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(medecin);
    }

    @GetMapping("/getAgentBiblio/{matricule}")
    public ResponseEntity<AgentBiblio> getAgentBiblio(@PathVariable String matricule){
        log.debug("request to get info etudiant");
        AgentBiblio agentBiblio = agentBiblioRepository.findByMatricule(matricule);
        if(agentBiblio == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(agentBiblio);
    }

    @GetMapping("/agent-scolarites/certificat")
    public void exportCertificat() {
        // PropertyConfigurator.configure("E:\\WORKSPACES\\java\\generationJasper\\log4j.properties");

        //Log in console in and log file
        // Requete pour la liste des niveaux
        // List<Niveau> niveaux = niveauRepository.findAll();
        String request_list_niveau ="SELECT departement.libelleLong, departement.codeDept \r\n" +
            "FROM departement \r\n" +
            "ORDER BY departement.codeDept";

        /** Requete pour le sous-rapport formations,
         * pour lister les formations appartenant à un département donné
         * List<Formation> formations = formationRepository.findAll(Departement departement)
         */
        String request_sub_formation="SELECT  formation.libelleLong, formation.codeFormation \r\n" +
            "FROM formation \r\n" +
            "where formation.departement=$P{codeDep}";

        /** Requete pour le sous-rapport niveau ,
         * pour lister les niveau appartenant à une formation donnée
         */
        String request_sub_niveau="SELECT niveau.libelleLong, COUNT(niveau.id)\r\n" +
            "FROM niveau	WHERE niveau.`formation` = $P{codeForm}\r\n" +
            "GROUP BY niveau.libelleLong";

        //		String request_list_eleve = "SELECT etudiant.nom,\r\n" +
        //				"	etudiant.prenom,\r\n" +
        //				"	etudiant.birthday_date,\r\n" +
        //				"	etudiant.`codeNiveau`\r\n" +
        //				"FROM etudiant\r\n" +
        //				"	INNER JOIN niveau ON \r\n" +
        //				"	 etudiant.niveau = niveau.niveau \r\n" +
        //				"	 AND etudiant.`codeNiveau` = niveau.`codeNiveau` ";

        String request_list_eleve_niveau = "SELECT etudiant.user.lastName,\r\n" +
            "	etudiant.niveau,\r\n" +
            "	etudiant.`codeNiveau`,\r\n" +
            "	etudiant.user.firstName,\r\n" +
            "	etudiant.`dateNaissance`\r\n" +
            "FROM etudiant\r\n" +
            "	INNER JOIN niveau ON \r\n" +
            "	 etudiant.niveau = niveau.libelleLong \r\n" +
            "	 AND etudiant.`codeNiveau` = niveau.`codeNiveau` \r\n" +
            "ORDER BY etudiant.niveau DESC";


        String request_cert = "SELECT etudiant.`numEtudiant`,\r\n" +
            "	etudiant.user.lastName,\r\n" +
            "	etudiant.user.firstName,\r\n" +
            "	etudiant.`dateNaissance`,\r\n" +
            "	etudiant.`lieuNaissance`,\r\n" +
            "	etudiant.nationalite,\r\n" +
            "	etudiant.niveau,\r\n" +
            "	etudiant.`niveauOption`,\r\n" +
            "	etudiant.annee_universitaire,\r\n" +
            "	etudiant.numero_certificat\r\n" +
            "FROM etudiant";

        //		String request_biblio="SELECT etudiant.nom,\r\n" +
        //				"	etudiant.prenom,\r\n" +
        //				"	etudiant.`codeNiveau`\r\n" +
        //				"FROM etudiant";
        //
        //
        String liste_niveau_source="src/main/java/sn/esp/inscription/jasper_files/listeNiveauFormation.jrxml";
        String liste_niveau_destination="liste_niveaux";

        //		String liste_eleve_source="jasper_files/list_eleves.jrxml";
        //		String liste_eleve_destination="jasper_generated_pdf/liste_eleves.pdf";
        //
        String liste_eleve_niveau_source="src/main/java/sn/esp/inscription/jasper_files/etudiantParNiveau.jrxml";
        String liste_eleve_niveau_destination="eleves_par_niveau";

        String formulaire_source="src/main/java/sn/esp/inscription/jasper_files/formulaire.jrxml";
        String formulaire_destination="formulaire";

        String certificat_source="src/main/java/sn/esp/inscription/jasper_files/jrxml";
        String certificat_destination="certificat";
        //
        //		String fileBu="jasper_files/formulaire.jrxml";
        //		String destBu = "jasper_generated_pdf/formulaire.pdf";
        //
        //

        try {

            // add the sub-reports of liste_niveau to a map object
            Map<String,String> subReportMap=new HashMap<String, String>();
            subReportMap.put(FORMATION_SUB, "src/main/java/sn/esp/inscription/jasper_files/formation.jrxml");
            subReportMap.put(NIVEAU_SUB, "src/main/java/sn/esp/inscription/jasper_files/niveau.jrxml");

            // add the requests of the sub-reports to a map object
            Map<String,String> subReportRequest=new HashMap<>();
            subReportRequest.put(FORMATION_SUB, request_sub_formation);
            subReportRequest.put(NIVEAU_SUB, request_sub_niveau);

            // add the sub-reports of liste_niveau to a map object
            Map<String,String> subReport2Map=new HashMap<String, String>();
            subReport2Map.put(FORMULAIRE_PAGE2_SUB, "src/main/java/sn/esp/inscription/jasper_files/formulairePage2.jrxml");

            pdfGenerator.generateReport("html",liste_niveau_source, liste_niveau_destination, request_list_niveau, new HashMap<String,Object>(),subReportMap,subReportRequest);
            pdfGenerator.generateReport("pdf",liste_niveau_source, liste_niveau_destination, request_list_niveau, new HashMap<String,Object>(),subReportMap,subReportRequest);

            pdfGenerator.generateReport("pdf",liste_eleve_niveau_source, liste_eleve_niveau_destination, request_list_eleve_niveau, null);

            pdfGenerator.generateReport("pdf",certificat_source, certificat_destination, request_cert, null);

            pdfGenerator.generateReport("pdf",formulaire_source, formulaire_destination,null, new HashMap<String,Object>(),subReport2Map,new HashMap<String,String>());

        } catch (ClassNotFoundException | JRException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
