package sn.esp.inscription.repository;

import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, String> {
    //Authority findByName(String user);
}
