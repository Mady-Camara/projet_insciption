package sn.esp.inscription.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.Departement;
import sn.esp.inscription.domain.enumeration.EnumTypeDept;

import java.util.List;

/**
 * Spring Data  repository for the Departement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DepartementRepository extends JpaRepository<Departement, Long> {
    Departement findByCodeDeptIgnoreCase(String codeDept);
    List<Departement> findByTypeDept(EnumTypeDept typeDept);
}
