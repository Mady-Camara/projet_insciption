package sn.esp.inscription.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.*;

import java.util.List;

/**
 * Spring Data  repository for the Inscription entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InscriptionRepository extends JpaRepository<Inscription, Long> {

    Inscription findById(long id);

    List<Inscription> findByNiveau(Niveau niveau); // Liste des inscrits d'un niveau
    List<Inscription> findByNiveauFormation(Formation formation); // Liste des inscrits dans une formation
    //***********  APTE **********
    List<Inscription> findByEtudiantAndEstApteTrue(Etudiant etudiant);
    List<Inscription> findByEtudiantAndEstApteFalse(Etudiant etudiant);
    List<Inscription> findByEtudiantAndEstApte(Etudiant etudiant, boolean estApte);

    //*********** Assurer *********
    List<Inscription> findByEstAssureTrue();
    List<Inscription> findByEstAssureFalse();

    //*********** Active *********



    //********* Regle Biblio *******
    List<Inscription> findByEtudiantAndEnRegleBiblioTrue(Etudiant etudiant);
    List<Inscription> findByEtudiantAndEnRegleBiblioFalse(Etudiant etudiant);

    //*********** Boursier **********
    List<Inscription> findByEtudiantAndEstBoursierTrue(Etudiant etudiant);
    List<Inscription> findByEtudiantAndEstBoursierFalse(Etudiant etudiant);

    //********* Les inscris *********
    List<Inscription> findByEtudiantAndEstApteTrueAndEstAssureTrueAndEnRegleBiblioTrue(Etudiant etudiant);//Liste des inscris

    List<Inscription> findByEtudiant(Etudiant etudiant);

    List<Inscription> findByAnneeUniversitaire(AnneeUniversitaire anneeUniversitaire);

    //List<Inscription> findByEtudiantAndEstApteTrue(Etudiant etudiant);
    Inscription findByEtudiantAndEstAssureTrue(Etudiant etudiant);
}
