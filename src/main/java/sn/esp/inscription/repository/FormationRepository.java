package sn.esp.inscription.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.Formation;

import java.util.List;

/**
 * Spring Data  repository for the Formation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FormationRepository extends JpaRepository<Formation, Long> {
    Formation findByCodeFormationIgnoreCase(String codeFormation);
    List<Formation> findByLibelleLong(String libelleLong);
}
