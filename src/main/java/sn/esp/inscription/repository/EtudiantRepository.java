package sn.esp.inscription.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.Etudiant;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.domain.enumeration.EnumSexe;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data  repository for the Etudiant entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EtudiantRepository extends JpaRepository<Etudiant, Long> {
    Etudiant findById(long id);
    List<Etudiant> findBySexe(EnumSexe sexe);
    Etudiant findByTelephone(String telephone);
    Etudiant findByNumIdentifiant(String numIdentifiant);
    Etudiant findByNumIdentifiantIgnoreCase(String numIdentifiant);
    //List<Etudiant> findByInscriptionEstApteTrue(); // Etudiants aptes
    Etudiant findByNumIdentifiantOrTelephone(String numIdentifiant, String telephone);

    Etudiant findByDateNaissance(LocalDate dateNaissance);
    Etudiant findByEmail(String email);

    Etudiant findByEmailIgnoreCase(String email);

    Etudiant findByUser(User user);
}
