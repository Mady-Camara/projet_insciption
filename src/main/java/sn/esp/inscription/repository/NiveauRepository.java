package sn.esp.inscription.repository;

import sn.esp.inscription.domain.Niveau;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Niveau entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NiveauRepository extends JpaRepository<Niveau, Long> {
    Niveau findByCodeNiveau(String codeNiveau);
    Niveau findByLibelleLong(String libelleLong);
}
