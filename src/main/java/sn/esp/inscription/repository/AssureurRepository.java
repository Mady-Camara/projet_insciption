package sn.esp.inscription.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.Assureur;
import sn.esp.inscription.domain.User;

/**
 * Spring Data  repository for the Assureur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssureurRepository extends JpaRepository<Assureur, Long> {
    Assureur findByMatricule(String matricule);

    Assureur findByUser(User user);
;}
