package sn.esp.inscription.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.AgentScolarite;

/**
 * Spring Data  repository for the AgentScolarite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentScolariteRepository extends JpaRepository<AgentScolarite, Long> {
    AgentScolarite findByMatricule(String matricule);
}
