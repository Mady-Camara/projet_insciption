package sn.esp.inscription.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.AnneeUniversitaire;

import java.util.List;

/**
 * Spring Data  repository for the AnneeUniversitaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnneeUniversitaireRepository extends JpaRepository<AnneeUniversitaire, Long> {
    AnneeUniversitaire findByLibelle(String libelle);
    List<AnneeUniversitaire> findByIsActiveTrue();
}
