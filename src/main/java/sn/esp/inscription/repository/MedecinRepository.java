package sn.esp.inscription.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.Medecin;
import sn.esp.inscription.domain.User;

/**
 * Spring Data  repository for the Medecin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedecinRepository extends JpaRepository<Medecin, Long> {
    Medecin findByMatricule(String matricule);

    Medecin findByUser(User user);
}
