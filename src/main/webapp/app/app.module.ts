import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import './vendor';
import { ProjetInscriptionSharedModule } from 'app/shared/shared.module';
import { ProjetInscriptionCoreModule } from 'app/core/core.module';
import { ProjetInscriptionAppRoutingModule } from './app-routing.module';
import { ProjetInscriptionHomeModule } from './home/home.module';
import { ProjetInscriptionEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { AideComponent } from './aide/aide.component';
import { ProjetInscriptionAppMaterielModule } from './app-materiel.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserModule,
    FontAwesomeModule,
    ProjetInscriptionSharedModule,
    ProjetInscriptionCoreModule,
    ProjetInscriptionHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ProjetInscriptionEntityModule,
    ProjetInscriptionAppRoutingModule,
    ProjetInscriptionAppMaterielModule,
    BrowserAnimationsModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent, AideComponent],
  bootstrap: [MainComponent],
})
export class ProjetInscriptionAppModule {}
