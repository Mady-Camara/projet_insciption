import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { SessionStorageService } from 'ngx-webstorage';
import { HttpResponse } from '@angular/common/http';

import { VERSION } from 'app/app.constants';
import { LANGUAGES } from 'app/core/language/language.constants';
import { AccountService } from 'app/core/auth/account.service';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { LoginService } from 'app/core/login/login.service';
import { ProfileService } from 'app/layouts/profiles/profile.service';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { IAssureur } from 'app/shared/model/assureur.model';
import { IMedecin } from 'app/shared/model/medecin.model';
import { IAgentBiblio } from 'app/shared/model/agent-biblio.model';
import { EtudiantService } from 'app/entities/etudiant/etudiant.service';
import { AssureurService } from 'app/entities/assureur/assureur.service';
import { MedecinService } from 'app/entities/medecin/medecin.service';
import { AgentBiblioService } from 'app/entities/agent-biblio/agent-biblio.service';

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['navbar.scss'],
})
export class NavbarComponent implements OnInit {
  etudiant: IEtudiant | null = null;
  assureur: IAssureur | null = null;
  medecin: IMedecin | null = null;
  agentBiblio: IAgentBiblio | null = null;
  inProduction?: boolean;
  isNavbarCollapsed = true;
  languages = LANGUAGES;
  swaggerEnabled?: boolean;
  version: string;
  etudiants?: IEtudiant[];
  // etudiantDashboard: string | null=null;

  constructor(
    private loginService: LoginService,
    private languageService: JhiLanguageService,
    private sessionStorage: SessionStorageService,
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private profileService: ProfileService,
    private router: Router,
    private etudiantServe: EtudiantService,
    private assureurService: AssureurService,
    private medecinService: MedecinService,
    private agentBiblioService: AgentBiblioService
  ) {
    this.version = VERSION ? (VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION) : '';
  }

  ngOnInit(): void {
    this.profileService.getProfileInfo().subscribe(profileInfo => {
      this.inProduction = profileInfo.inProduction;
      this.swaggerEnabled = profileInfo.swaggerEnabled;
    });
    // this.etudiantDashboard = "/etudiant/"+this.etudiant?.id+"/dashboard";
  }

  etudiantId(): void {
    this.etudiantServe.findEtudiantId().subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
    this.router.navigate(['/etudiant', this.etudiant?.id, 'dashboard']);
  }

  assureurId(): void {
    this.assureurService.findAssureurId().subscribe((res: HttpResponse<IAssureur>) => (this.assureur = res.body));
    this.router.navigate(['/assureur', this.assureur?.id, 'dashboard']);
  }

  medecinId(): void {
    this.medecinService.findMedecinId().subscribe((res: HttpResponse<IMedecin>) => (this.medecin = res.body));
    this.router.navigate(['/medecin', this.medecin?.id, 'dashboard']);
  }

  agentBiblioId(): void {
    this.agentBiblioService.findAgentBiblioId().subscribe((res: HttpResponse<IAgentBiblio>) => (this.agentBiblio = res.body));
    this.router.navigate(['/agent-biblio', this.agentBiblio?.id, 'dashboard']);
  }

  changeLanguage(languageKey: string): void {
    this.sessionStorage.store('locale', languageKey);
    this.languageService.changeLanguage(languageKey);
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  logout(): void {
    this.collapseNavbar();
    this.loginService.logout();
    this.router.navigate(['']);
  }

  toggleNavbar(): void {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  getImageUrl(): string {
    return this.isAuthenticated() ? this.accountService.getImageUrl() : '';
  }
}
