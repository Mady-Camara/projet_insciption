import { NgModule } from '@angular/core';
import { ProjetInscriptionSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
// import { ProjetInscriptionAppMaterielModule} from '../app-materiel.module';
// import { EtudiantsComponent } from '../etudiants/etudiants.component';

@NgModule({
  imports: [ProjetInscriptionSharedLibsModule /* ProjetInscriptionAppMaterielModule */],
  declarations: [
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective /* EtudiantsComponent */,
  ],
  entryComponents: [LoginModalComponent],
  exports: [
    ProjetInscriptionSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    // ProjetInscriptionAppMaterielModule
  ],
})
export class ProjetInscriptionSharedModule {}
