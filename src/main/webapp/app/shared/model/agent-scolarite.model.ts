import { IUser } from 'app/core/user/user.model';

export interface IAgentScolarite {
  id?: number;
  matricule?: string;
  user?: IUser;
}

export class AgentScolarite implements IAgentScolarite {
  constructor(public id?: number, public matricule?: string, public user?: IUser) {}
}
