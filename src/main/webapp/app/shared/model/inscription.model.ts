import { INiveau } from 'app/shared/model/niveau.model';
import { IAnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { EnumSituation } from 'app/shared/model/enumerations/enum-situation.model';
import { EnumNombreInscriptionAnterieure } from 'app/shared/model/enumerations/enum-nombre-inscription-anterieure.model';
import { EnumHoraireTd } from 'app/shared/model/enumerations/enum-horaire-td.model';

export interface IInscription {
  id?: number;
  estApte?: boolean;
  estBoursier?: boolean;
  estAssure?: boolean;
  enRegleBiblio?: boolean;
  situationMatrimoniale?: EnumSituation;
  anneeEtude?: string;
  cycle?: number;
  departement?: string;
  optionChoisie?: string;
  nombreInscriptionAnterieur?: EnumNombreInscriptionAnterieure;
  redoublezVous?: boolean;
  horaireDesTd?: EnumHoraireTd;
  niveau?: INiveau;
  anneeUniversitaire?: IAnneeUniversitaire;
  etudiant?: IEtudiant;
}

export class Inscription implements IInscription {
  constructor(
    public id?: number,
    public estApte?: boolean,
    public estBoursier?: boolean,
    public estAssure?: boolean,
    public enRegleBiblio?: boolean,
    public situationMatrimoniale?: EnumSituation,
    public anneeEtude?: string,
    public cycle?: number,
    public departement?: string,
    public optionChoisie?: string,
    public nombreInscriptionAnterieur?: EnumNombreInscriptionAnterieure,
    public redoublezVous?: boolean,
    public horaireDesTd?: EnumHoraireTd,
    public niveau?: INiveau,
    public anneeUniversitaire?: IAnneeUniversitaire,
    public etudiant?: IEtudiant
  ) {
    this.estApte = this.estApte || false;
    this.estBoursier = this.estBoursier || false;
    this.estAssure = this.estAssure || false;
    this.enRegleBiblio = this.enRegleBiblio || false;
    this.redoublezVous = this.redoublezVous || false;
  }
}
