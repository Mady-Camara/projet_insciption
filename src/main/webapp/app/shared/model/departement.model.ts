import { IFormation } from 'app/shared/model/formation.model';
import { EnumTypeDept } from 'app/shared/model/enumerations/enum-type-dept.model';

export interface IDepartement {
  id?: number;
  codeDept?: string;
  libelleLong?: string;
  chef?: string;
  typeDept?: EnumTypeDept;
  departements?: IFormation[];
}

export class Departement implements IDepartement {
  constructor(
    public id?: number,
    public codeDept?: string,
    public libelleLong?: string,
    public chef?: string,
    public typeDept?: EnumTypeDept,
    public departements?: IFormation[]
  ) {}
}
