import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IInscription } from 'app/shared/model/inscription.model';
import { EnumSexe } from 'app/shared/model/enumerations/enum-sexe.model';
import { EnumBourse } from 'app/shared/model/enumerations/enum-bourse.model';
import { EnumNatureBourse } from 'app/shared/model/enumerations/enum-nature-bourse.model';
import { EnumStatusEtudiant } from 'app/shared/model/enumerations/enum-status-etudiant.model';

export interface IEtudiant {
  id?: number;
  numIdentifiant?: string;
  dateNaissance?: Moment;
  lieuNaissance?: string;
  sexe?: EnumSexe;
  ine?: string;
  telephone?: string;
  email?: string;
  nomDuMari?: string;
  regionDeNaissance?: string;
  prenom2?: string;
  prenom3?: string;
  paysDeNaissance?: string;
  nationalite?: string;
  addresseADakar?: string;
  boitePostal?: string;
  portable?: string;
  activiteSalariee?: boolean;
  categorieSocioprofessionnelle?: string;
  situationFamiliale?: string;
  nombreEnfant?: number;
  bourse?: EnumBourse;
  natureBourse?: EnumNatureBourse;
  montantBourse?: number;
  organismeBoursier?: string;
  serieDiplome?: string;
  anneeDiplome?: string;
  mentionDiplome?: string;
  lieuDiplome?: string;
  serieDuelDuesDutBts?: string;
  anneeDuelDuesDutBts?: string;
  mentionDuelDuesDutBts?: string;
  lieuDuelDuesDutBts?: string;
  serieLicenceComplete?: string;
  anneeLicenceComplete?: string;
  mentionLicenceComplete?: string;
  lieuLicenceComplete?: string;
  serieMaster?: string;
  anneeMaster?: string;
  mentionMaster?: string;
  lieu?: string;
  serieDoctorat?: string;
  anneeDoctorat?: string;
  mentionDoctorat?: string;
  lieuDoctorat?: string;
  nomPersonneAContacter?: string;
  nomDuMariPersonneAContacter?: string;
  lienDeParentePersonneAContacter?: string;
  adressePersonneAContacter?: string;
  rueQuartierPersonneAContacter?: string;
  villePersonneAContacter?: string;
  telephonePersonneAContacter?: string;
  prenomPersonneAContacter?: string;
  prenom2PersonneAContacter?: string;
  prenom3PersonneAContacter?: string;
  boitePostalePersonneAContacter?: string;
  portPersonneAContacter?: string;
  faxPersonneAContacter?: string;
  emialPersonneAContacter?: string;
  responsableEstEtudiant?: boolean;
  personneAContacter?: boolean;
  statusEtudiant?: EnumStatusEtudiant;
  user?: IUser;
  etudiants?: IInscription[];
}

export class Etudiant implements IEtudiant {
  constructor(
    public id?: number,
    public numIdentifiant?: string,
    public dateNaissance?: Moment,
    public lieuNaissance?: string,
    public sexe?: EnumSexe,
    public ine?: string,
    public telephone?: string,
    public email?: string,
    public nomDuMari?: string,
    public regionDeNaissance?: string,
    public prenom2?: string,
    public prenom3?: string,
    public paysDeNaissance?: string,
    public nationalite?: string,
    public addresseADakar?: string,
    public boitePostal?: string,
    public portable?: string,
    public activiteSalariee?: boolean,
    public categorieSocioprofessionnelle?: string,
    public situationFamiliale?: string,
    public nombreEnfant?: number,
    public bourse?: EnumBourse,
    public natureBourse?: EnumNatureBourse,
    public montantBourse?: number,
    public organismeBoursier?: string,
    public serieDiplome?: string,
    public anneeDiplome?: string,
    public mentionDiplome?: string,
    public lieuDiplome?: string,
    public serieDuelDuesDutBts?: string,
    public anneeDuelDuesDutBts?: string,
    public mentionDuelDuesDutBts?: string,
    public lieuDuelDuesDutBts?: string,
    public serieLicenceComplete?: string,
    public anneeLicenceComplete?: string,
    public mentionLicenceComplete?: string,
    public lieuLicenceComplete?: string,
    public serieMaster?: string,
    public anneeMaster?: string,
    public mentionMaster?: string,
    public lieu?: string,
    public serieDoctorat?: string,
    public anneeDoctorat?: string,
    public mentionDoctorat?: string,
    public lieuDoctorat?: string,
    public nomPersonneAContacter?: string,
    public nomDuMariPersonneAContacter?: string,
    public lienDeParentePersonneAContacter?: string,
    public adressePersonneAContacter?: string,
    public rueQuartierPersonneAContacter?: string,
    public villePersonneAContacter?: string,
    public telephonePersonneAContacter?: string,
    public prenomPersonneAContacter?: string,
    public prenom2PersonneAContacter?: string,
    public prenom3PersonneAContacter?: string,
    public boitePostalePersonneAContacter?: string,
    public portPersonneAContacter?: string,
    public faxPersonneAContacter?: string,
    public emialPersonneAContacter?: string,
    public responsableEstEtudiant?: boolean,
    public personneAContacter?: boolean,
    public statusEtudiant?: EnumStatusEtudiant,
    public user?: IUser,
    public etudiants?: IInscription[]
  ) {
    this.activiteSalariee = this.activiteSalariee || false;
    this.responsableEstEtudiant = this.responsableEstEtudiant || false;
    this.personneAContacter = this.personneAContacter || false;
  }
}
