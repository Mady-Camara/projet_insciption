import { IUser } from 'app/core/user/user.model';

export interface IMedecin {
  id?: number;
  matricule?: string;
  email?: string;
  password?: string;
  user?: IUser;
}

export class Medecin implements IMedecin {
  constructor(public id?: number, public matricule?: string, public email?: string, public password?: string, public user?: IUser) {}
}
