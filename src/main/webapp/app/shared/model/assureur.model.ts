import { IUser } from 'app/core/user/user.model';

export interface IAssureur {
  id?: number;
  matricule?: string;
  email?: string;
  password?: string;
  user?: IUser;
}

export class Assureur implements IAssureur {
  constructor(public id?: number, public matricule?: string, public email?: string, public password?: string, public user?: IUser) {}
}
