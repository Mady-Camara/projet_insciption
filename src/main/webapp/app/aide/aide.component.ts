import { Component, OnInit } from '@angular/core';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'jhi-aide',
  templateUrl: './aide.component.html',
  styleUrls: ['./aide.scss'],
})
export class AideComponent implements OnInit {
  public isCollapsed = true;
  public isCollapsedTwo = false;
  public isCollapsedThree = false;
  faQuestionCircle = faQuestionCircle;

  partOne: string;
  partTwo: string;
  partThree: string;
  linkcolor: string;
  linkcolortwo: string;
  linkcolorThree: string;
  iconcolor: string;
  iconcolortwo: string;
  iconcolorThree: string;

  constructor() {
    this.partOne = '#74D0F1';
    this.partTwo = '#EFEFEF';
    this.partThree = '#EFEFEF';
    this.linkcolor = 'white';
    this.linkcolortwo = '#318CE7';
    this.linkcolorThree = '#318CE7';
    this.iconcolor = 'green';
    this.iconcolortwo = 'gray';
    this.iconcolorThree = 'gray';
  }

  ngOnInit(): void {}

  onClick(): void {
    this.isCollapsed = false;
    this.isCollapsedTwo = true;
    this.isCollapsedThree = true;
    if (this.partOne === '#74D0F1') {
      this.partOne = '#74D0F1';
      this.partTwo = '#EFEFEF';
      this.partThree = '#EFEFEF';
      this.linkcolor = 'white';
      this.linkcolortwo = '#318CE7';
      this.linkcolorThree = '#318CE7';
      this.iconcolor = 'green';
      this.iconcolortwo = 'gray';
      this.iconcolorThree = 'gray';
    } else {
      this.partOne = '#74D0F1';
      this.partTwo = '#EFEFEF';
      this.partThree = '#EFEFEF';
      this.linkcolor = 'white';
      this.linkcolortwo = '#318CE7';
      this.linkcolorThree = '#318CE7';
      this.iconcolor = 'green';
      this.iconcolortwo = 'gray';
      this.iconcolorThree = 'gray';
    }
  }

  onClicktwo(): void {
    this.isCollapsedTwo = true;
    this.isCollapsed = false;
    this.isCollapsedThree = false;
    if (this.partTwo === '#74D0F1') {
      this.partTwo = '#74D0F1';
      this.partOne = '#EFEFEF';
      this.partThree = '#EFEFEF';
      this.linkcolortwo = 'white';
      this.linkcolor = '#318CE7';
      this.linkcolorThree = '#318CE7';
      this.iconcolortwo = 'green';
      this.iconcolor = 'gray';
      this.iconcolorThree = 'gray';
    } else {
      this.partTwo = '#74D0F1';
      this.partOne = '#EFEFEF';
      this.partThree = '#EFEFEF';
      this.linkcolortwo = 'white';
      this.linkcolor = '#318CE7';
      this.linkcolorThree = '#318CE7';
      this.iconcolortwo = 'green';
      this.iconcolor = 'gray';
      this.iconcolorThree = 'gray';
    }
  }

  onClickThree(): void {
    this.isCollapsedThree = true;
    this.isCollapsedTwo = false;
    this.isCollapsed = false;
    if (this.partThree === '#74D0F1') {
      this.partThree = '#74D0F1';
      this.partOne = '#EFEFEF';
      this.partTwo = '#EFEFEF';
      this.linkcolorThree = 'white';
      this.linkcolor = '#318CE7';
      this.linkcolortwo = '#318CE7';
      this.iconcolorThree = 'green';
      this.iconcolor = 'gray';
      this.iconcolortwo = 'gray';
    } else {
      this.partThree = '#74D0F1';
      this.partOne = '#EFEFEF';
      this.partTwo = '#EFEFEF';
      this.linkcolorThree = 'white';
      this.linkcolor = '#318CE7';
      this.linkcolortwo = '#318CE7';
      this.iconcolorThree = 'green';
      this.iconcolor = 'gray';
      this.iconcolortwo = 'gray';
    }
  }
}
