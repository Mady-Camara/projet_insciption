import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetInscriptionSharedModule } from 'app/shared/shared.module';
import { AgentScolariteComponent } from './agent-scolarite.component';
import { AgentScolariteDetailComponent } from './agent-scolarite-detail.component';
import { AgentScolariteUpdateComponent } from './agent-scolarite-update.component';
import { AgentScolariteDeleteDialogComponent } from './agent-scolarite-delete-dialog.component';
import { agentScolariteRoute } from './agent-scolarite.route';

@NgModule({
  imports: [ProjetInscriptionSharedModule, RouterModule.forChild(agentScolariteRoute)],
  declarations: [
    AgentScolariteComponent,
    AgentScolariteDetailComponent,
    AgentScolariteUpdateComponent,
    AgentScolariteDeleteDialogComponent,
  ],
  entryComponents: [AgentScolariteDeleteDialogComponent],
})
export class ProjetInscriptionAgentScolariteModule {}
