import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentScolarite } from 'app/shared/model/agent-scolarite.model';
import { AgentScolariteService } from './agent-scolarite.service';

@Component({
  templateUrl: './agent-scolarite-delete-dialog.component.html',
})
export class AgentScolariteDeleteDialogComponent {
  agentScolarite?: IAgentScolarite;

  constructor(
    protected agentScolariteService: AgentScolariteService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.agentScolariteService.delete(id).subscribe(() => {
      this.eventManager.broadcast('agentScolariteListModification');
      this.activeModal.close();
    });
  }
}
