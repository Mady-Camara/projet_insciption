import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentScolarite } from 'app/shared/model/agent-scolarite.model';

@Component({
  selector: 'jhi-agent-scolarite-dashboard',
  templateUrl: './agent-scolarite-dashboard.component.html',
})
export class AgentScolariteDashboardComponent implements OnInit {
  agentScolarite: IAgentScolarite | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentScolarite }) => (this.agentScolarite = agentScolarite));
  }

  previousState(): void {
    window.history.back();
  }
}
