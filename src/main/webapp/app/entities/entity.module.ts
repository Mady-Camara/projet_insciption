import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'etudiant',
        loadChildren: () => import('./etudiant/etudiant.module').then(m => m.ProjetInscriptionEtudiantModule),
      },
      {
        path: 'assureur',
        loadChildren: () => import('./assureur/assureur.module').then(m => m.ProjetInscriptionAssureurModule),
      },
      {
        path: 'agent-biblio',
        loadChildren: () => import('./agent-biblio/agent-biblio.module').then(m => m.ProjetInscriptionAgentBiblioModule),
      },
      {
        path: 'medecin',
        loadChildren: () => import('./medecin/medecin.module').then(m => m.ProjetInscriptionMedecinModule),
      },
      {
        path: 'agent-scolarite',
        loadChildren: () => import('./agent-scolarite/agent-scolarite.module').then(m => m.ProjetInscriptionAgentScolariteModule),
      },
      {
        path: 'departement',
        loadChildren: () => import('./departement/departement.module').then(m => m.ProjetInscriptionDepartementModule),
      },
      {
        path: 'inscription',
        loadChildren: () => import('./inscription/inscription.module').then(m => m.ProjetInscriptionInscriptionModule),
      },
      {
        path: 'formation',
        loadChildren: () => import('./formation/formation.module').then(m => m.ProjetInscriptionFormationModule),
      },
      {
        path: 'annee-universitaire',
        loadChildren: () =>
          import('./annee-universitaire/annee-universitaire.module').then(m => m.ProjetInscriptionAnneeUniversitaireModule),
      },
      {
        path: 'niveau',
        loadChildren: () => import('./niveau/niveau.module').then(m => m.ProjetInscriptionNiveauModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ProjetInscriptionEntityModule {}
