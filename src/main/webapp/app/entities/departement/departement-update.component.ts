import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IDepartement, Departement } from 'app/shared/model/departement.model';
import { DepartementService } from './departement.service';

@Component({
  selector: 'jhi-departement-update',
  templateUrl: './departement-update.component.html',
})
export class DepartementUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    codeDept: [null, [Validators.required, Validators.minLength(2)]],
    libelleLong: [null, [Validators.required]],
    chef: [null, [Validators.required]],
    typeDept: [null, [Validators.required]],
  });

  constructor(protected departementService: DepartementService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ departement }) => {
      this.updateForm(departement);
    });
  }

  updateForm(departement: IDepartement): void {
    this.editForm.patchValue({
      id: departement.id,
      codeDept: departement.codeDept,
      libelleLong: departement.libelleLong,
      chef: departement.chef,
      typeDept: departement.typeDept,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const departement = this.createFromForm();
    if (departement.id !== undefined) {
      this.subscribeToSaveResponse(this.departementService.update(departement));
    } else {
      this.subscribeToSaveResponse(this.departementService.create(departement));
    }
  }

  private createFromForm(): IDepartement {
    return {
      ...new Departement(),
      id: this.editForm.get(['id'])!.value,
      codeDept: this.editForm.get(['codeDept'])!.value,
      libelleLong: this.editForm.get(['libelleLong'])!.value,
      chef: this.editForm.get(['chef'])!.value,
      typeDept: this.editForm.get(['typeDept'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDepartement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
