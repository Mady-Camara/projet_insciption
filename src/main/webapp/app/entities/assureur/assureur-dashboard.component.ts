import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAssureur } from 'app/shared/model/assureur.model';

import { IEtudiant } from 'app/shared/model/etudiant.model';
import { IInscription } from 'app/shared/model/inscription.model';
import { EtudiantService } from 'app/entities/etudiant/etudiant.service';
import { AssureurService } from 'app/entities/assureur/assureur.service';
import { InscriptionService } from 'app/entities/inscription/inscription.service';
import { HttpResponse } from '@angular/common/http';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'jhi-assureur-dashboard',
  templateUrl: './assureur-dashboard.component.html',
})
export class AssureurDashboardComponent implements OnInit {
  assureur: IAssureur | null = null;
  etudiant: IEtudiant | null = null;
  inscription: IInscription | null = null;

  search = new FormControl();
  isDone = false;
  isNotDone = false;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected service: EtudiantService,
    protected serve: AssureurService,
    protected inservice: InscriptionService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assureur }) => (this.assureur = assureur));
  }

  previousState(): void {
    window.history.back();
  }

  rechercher(): void {
    this.etudiant = null;
    this.isDone = false;
    this.isNotDone = false;
    this.service.findEtudiant(this.search.value).subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
    this.inservice.etat(this.search.value).subscribe((res: HttpResponse<IInscription>) => (this.inscription = res.body));
  }

  payer(): void {
    if (this.etudiant != null) {
      this.serve.payer(this.etudiant).subscribe((res: HttpResponse<IInscription>) => (this.inscription = res.body));
      this.isDone = true;
    } else {
      this.isNotDone = true;
    }
  }
}
