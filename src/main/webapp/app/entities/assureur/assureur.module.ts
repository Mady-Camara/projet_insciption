import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetInscriptionSharedModule } from 'app/shared/shared.module';
import { AssureurComponent } from './assureur.component';
import { AssureurDetailComponent } from './assureur-detail.component';
import { AssureurDashboardComponent } from './assureur-dashboard.component';
import { AssureurUpdateComponent } from './assureur-update.component';
import { AssureurDeleteDialogComponent } from './assureur-delete-dialog.component';
import { assureurRoute } from './assureur.route';

@NgModule({
  imports: [ProjetInscriptionSharedModule, RouterModule.forChild(assureurRoute)],
  declarations: [
    AssureurComponent,
    AssureurDetailComponent,
    AssureurDashboardComponent,
    AssureurUpdateComponent,
    AssureurDeleteDialogComponent,
  ],
  entryComponents: [AssureurDeleteDialogComponent],
})
export class ProjetInscriptionAssureurModule {}
