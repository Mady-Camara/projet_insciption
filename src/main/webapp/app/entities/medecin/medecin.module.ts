import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetInscriptionSharedModule } from 'app/shared/shared.module';
import { MedecinComponent } from './medecin.component';
import { MedecinDetailComponent } from './medecin-detail.component';
import { MedecinDashboardComponent } from './medecin-dashboard.component';
import { MedecinUpdateComponent } from './medecin-update.component';
import { MedecinDeleteDialogComponent } from './medecin-delete-dialog.component';
import { medecinRoute } from './medecin.route';

@NgModule({
  imports: [ProjetInscriptionSharedModule, RouterModule.forChild(medecinRoute)],
  declarations: [MedecinComponent, MedecinDetailComponent, MedecinDashboardComponent, MedecinUpdateComponent, MedecinDeleteDialogComponent],
  entryComponents: [MedecinDeleteDialogComponent],
})
export class ProjetInscriptionMedecinModule {}
