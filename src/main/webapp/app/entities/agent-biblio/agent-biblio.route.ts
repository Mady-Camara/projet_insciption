import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAgentBiblio, AgentBiblio } from 'app/shared/model/agent-biblio.model';
import { AgentBiblioService } from './agent-biblio.service';
import { AgentBiblioComponent } from './agent-biblio.component';
import { AgentBiblioDetailComponent } from './agent-biblio-detail.component';
import { AgentBiblioUpdateComponent } from './agent-biblio-update.component';
import { AgentBiblioDashboardComponent } from './agent-biblio-dashboard.component';

@Injectable({ providedIn: 'root' })
export class AgentBiblioResolve implements Resolve<IAgentBiblio> {
  constructor(private service: AgentBiblioService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAgentBiblio> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((agentBiblio: HttpResponse<AgentBiblio>) => {
          if (agentBiblio.body) {
            return of(agentBiblio.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AgentBiblio());
  }
}

export const agentBiblioRoute: Routes = [
  {
    path: '',
    component: AgentBiblioComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'projetInscriptionApp.agentBiblio.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AgentBiblioDetailComponent,
    resolve: {
      agentBiblio: AgentBiblioResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'projetInscriptionApp.agentBiblio.home.title',
    },
    canActivate: [UserRouteAccessService],
  },

  {
    path: ':id/dashboard',
    component: AgentBiblioDashboardComponent,
    resolve: {
      agentBiblio: AgentBiblioResolve,
    },
    data: {
      authorities: [Authority.AGENTBIBLIO],
      pageTitle: 'projetInscriptionApp.agentBiblio.home.title',
    },
    canActivate: [UserRouteAccessService],
  },

  {
    path: 'new',
    component: AgentBiblioUpdateComponent,
    resolve: {
      agentBiblio: AgentBiblioResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'projetInscriptionApp.agentBiblio.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AgentBiblioUpdateComponent,
    resolve: {
      agentBiblio: AgentBiblioResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'projetInscriptionApp.agentBiblio.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
