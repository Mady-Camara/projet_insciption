import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetInscriptionSharedModule } from 'app/shared/shared.module';
import { AgentBiblioComponent } from './agent-biblio.component';
import { AgentBiblioDetailComponent } from './agent-biblio-detail.component';
import { AgentBiblioDashboardComponent } from './agent-biblio-dashboard.component';
import { AgentBiblioUpdateComponent } from './agent-biblio-update.component';
import { AgentBiblioDeleteDialogComponent } from './agent-biblio-delete-dialog.component';
import { agentBiblioRoute } from './agent-biblio.route';

@NgModule({
  imports: [ProjetInscriptionSharedModule, RouterModule.forChild(agentBiblioRoute)],
  declarations: [
    AgentBiblioComponent,
    AgentBiblioDetailComponent,
    AgentBiblioDashboardComponent,
    AgentBiblioUpdateComponent,
    AgentBiblioDeleteDialogComponent,
  ],
  entryComponents: [AgentBiblioDeleteDialogComponent],
})
export class ProjetInscriptionAgentBiblioModule {}
