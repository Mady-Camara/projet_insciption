import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentBiblio } from 'app/shared/model/agent-biblio.model';
import { FormControl } from '@angular/forms';
import { EtudiantService } from 'app/entities/etudiant/etudiant.service';
import { HttpResponse } from '@angular/common/http';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { AgentBiblioService } from 'app/entities/agent-biblio/agent-biblio.service';
import { IInscription } from 'app/shared/model/inscription.model';

@Component({
  selector: 'jhi-agent-biblio-dashboard',
  templateUrl: './agent-biblio-dashboard.component.html',
})
export class AgentBiblioDashboardComponent implements OnInit {
  agentBiblio: IAgentBiblio | null = null;
  etudiant: IEtudiant | null = null;
  inscription: IInscription | null = null;

  search = new FormControl();
  isDone = false;

  constructor(protected activatedRoute: ActivatedRoute, protected service: EtudiantService, protected serve: AgentBiblioService) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentBiblio }) => (this.agentBiblio = agentBiblio));
  }

  previousState(): void {
    window.history.back();
  }

  rechercher(): void {
    this.service.findEtudiant(this.search.value).subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
  }

  regler(): void {
    if (this.etudiant != null) {
      this.serve.regler(this.etudiant).subscribe((res: HttpResponse<IInscription>) => (this.inscription = res.body));
      this.isDone = true;
    }
  }
}
