import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetInscriptionSharedModule } from 'app/shared/shared.module';
import { AnneeUniversitaireComponent } from './annee-universitaire.component';
import { AnneeUniversitaireDetailComponent } from './annee-universitaire-detail.component';
import { AnneeUniversitaireUpdateComponent } from './annee-universitaire-update.component';
import { AnneeUniversitaireDeleteDialogComponent } from './annee-universitaire-delete-dialog.component';
import { anneeUniversitaireRoute } from './annee-universitaire.route';

@NgModule({
  imports: [ProjetInscriptionSharedModule, RouterModule.forChild(anneeUniversitaireRoute)],
  declarations: [
    AnneeUniversitaireComponent,
    AnneeUniversitaireDetailComponent,
    AnneeUniversitaireUpdateComponent,
    AnneeUniversitaireDeleteDialogComponent,
  ],
  entryComponents: [AnneeUniversitaireDeleteDialogComponent],
})
export class ProjetInscriptionAnneeUniversitaireModule {}
