import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'jhi-etudiant-dashboard',
  templateUrl: './etudiant-dashboard.component.html',
  styleUrls: ['./etudiants.scss'],
})
export class EtudiantDashboardComponent implements OnInit {
  firstFormGroup = this._formBuilder.group({
    firstCtrl: [''],
    testCtrl: [''],
    nameMari: [''],
  });

  secondFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
    testCtrl: ['', Validators.required],
    nameMari: [''],
    dateNaiss: ['', Validators.required],
    sexe: ['', Validators.required],
    lieuNaiss: ['', Validators.required],
    paysNaiss: ['', Validators.required],
    regionNaiss: ['', Validators.required],
    nationalite: ['', Validators.required],
  });

  thirdFormGroup = this._formBuilder.group({
    adresse: ['', Validators.required],
    boitePostale: [''],
    telephone: ['', Validators.required],
    portable: ['', Validators.required],
    email: ['', Validators.required],
  });

  forFormGroup = this._formBuilder.group({
    actSalarieExist: ['', Validators.required],
    statutEtudiant: ['', Validators.required],
    catProfession: [''],
  });

  fiveFormGroup = this._formBuilder.group({
    sitFamiliale: [''],
    nbrEnfant: [''],
  });

  sixFormGroup = this._formBuilder.group({
    anneEtude: ['', Validators.required],
    cycle: ['', Validators.required],
    departement: ['', Validators.required],
    nbrInscriptionAnt: ['', Validators.required],
    redoublerExist: ['', Validators.required],
    horaireTD: [''],
  });

  sevenFormGroup = this._formBuilder.group({
    bourse: ['', Validators.required],
    typeBourse: [''],
    montantBourse: [''],
    organismeBousier: [''],
    pourcentageExonore: [''],
  });

  heightFormGroup = this._formBuilder.group({
    adresse: ['', Validators.required],
    boitePostale: [''],
    telephone: ['', Validators.required],
    portable: [''],
    email: ['', Validators.required],
  });

  nineFormGroup = this._formBuilder.group({
    nomResponsable: ['', Validators.required],
    prenomResponsable: ['', Validators.required],
    nomMariResponsable: [''],
    lienParente: ['', Validators.required],
    rue: ['', Validators.required],
    quartier: ['', Validators.required],
    boitePostale: [''],
    telephone: ['', Validators.required],
    portable: [''],
    fax: [''],
    email: ['', Validators.required],
    resEtudiantTF: ['', Validators.required],
    resAContacterTF: ['', Validators.required],
  });

  tenFormGroup = this._formBuilder.group({
    isApte: [''],
  });
  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit(): void {}
}
