import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetInscriptionSharedModule } from 'app/shared/shared.module';
import { EtudiantComponent } from './etudiant.component';
import { EtudiantDetailComponent } from './etudiant-detail.component';
import { EtudiantDashboardComponent } from './etudiant-dashboard.component';
import { EtudiantUpdateComponent } from './etudiant-update.component';
import { EtudiantDeleteDialogComponent } from './etudiant-delete-dialog.component';
import { etudiantRoute } from './etudiant.route';

import '../../vendor';
import { ProjetInscriptionAppMaterielModule } from '../../app-materiel.module';

@NgModule({
  imports: [ProjetInscriptionSharedModule, RouterModule.forChild(etudiantRoute), ProjetInscriptionAppMaterielModule],
  declarations: [
    EtudiantComponent,
    EtudiantDetailComponent,
    EtudiantDashboardComponent,
    EtudiantUpdateComponent,
    EtudiantDeleteDialogComponent,
  ],
  entryComponents: [EtudiantDeleteDialogComponent],
})
export class ProjetInscriptionEtudiantModule {}
